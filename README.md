\mainpage Std

The standard `Hz` library is a library of general purpose components consumed by other parts of the library. This library is part of the `Hz` library, and is contained within that namespace.

 - [Code](https://gitlab.com/seppeon/std)
 - [Doco](https://seppeon.gitlab.io/std/)

# Installation

To use the library:

1. Clone the repo and open the cloned directory:
```
git clone https://gitlab.com/seppeon/std.git
cd std
```

2. Create a `build` directory, and enter that directory:
```
mkdir build
cd build
```

3. Install the dependancies (`catch2`) using `conan`:
```
conan install .. --profile:build=mingw --profile:host=mingw --build=missing -s build_type=Release
```

4. This package is not yet in conan center, so to install it locally (for consumption use with conan) run:
```
conan create .. --profile:build=mingw --profile:host=mingw --build=missing -s build_type=Release
```

5. Open the project which you want to consume this library:
  - Add the `hz_std/0.1.1` dependancy to your conanfile.py, or conanfile.txt (or whatever version you want).
  - Edit your `CMakeLists.txt` file, and you should be able to find the library:
  ```CMAKE
  find_package(TL REQUIRED)
  add_library(ExampleLibrary)
  target_link_libraries(ExampleLibrary PUBLIC Hz::Std)
  ```
  - Run the conan install command, which will install the dependancy for your project.