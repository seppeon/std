#pragma once
#include "Hz/Std/Apply.hpp"
#include "Hz/Std/AlwaysInline.hpp"
#include <Hz/TL/TLAtIndex.hpp>
#include <Hz/TL/TLIndexOf.hpp>
#include <Hz/TL/TLInsertAt.hpp>
#include <Hz/TL/TLTranslate.hpp>
#include <Hz/TL/TLReplaceAt.hpp>
#include <Hz/TL/TLRemoveAt.hpp>
#include <Hz/TL/TLCombine.hpp>
#include <Hz/Traits/Fwd.hpp>
#include <utility>
#include <cstddef>

namespace Hz
{
	// Aggregate constructible tuple
	template <typename ... Args>
	struct AgTup;
	template <typename T, size_t>
	struct AgTupElem;
	template <typename Is, typename...Args>
	struct AgTupElems;

	template <typename L, typename Match>
	inline constexpr size_t AgTupIndexOf = TLIndexOf<L, Match>;
	template <typename Is, typename...Args, typename Match>
	inline constexpr size_t AgTupIndexOf<AgTupElems<Is, Args...>, Match> = TLIndexOf<TL<Args...>, Match>;
}

namespace std
{
	template< std::size_t I, class... Args >
	struct tuple_element<I, ::Hz::AgTup<Args...>> { using type = ::Hz::TLAtIndex<::Hz::TL<Args...>, I>; };
	template< std::size_t I, class Meh, class... Args >
	struct tuple_element<I, ::Hz::AgTupElems<Meh, Args...>> { using type = ::Hz::TLAtIndex<::Hz::TL<Args...>, I>; };

	template<class ... Args>
	struct tuple_size<::Hz::AgTup<Args...>> : std::integral_constant<std::size_t, sizeof...(Args)> {};
	template<class Meh, class ... Args>
	struct tuple_size<::Hz::AgTupElems<Meh, Args...>> : std::integral_constant<std::size_t, sizeof...(Args)> {};
}

namespace Hz
{
	template <typename T, size_t>
	struct AgTupElem { T value; };
	template <size_t I>
	struct AgTupElem<void, I>{};
	template <size_t ... Is, typename ... Args>
	struct AgTupElems<std::index_sequence<Is...>, Args...> : AgTupElem<Args, Is> ...
	{
		using self = AgTupElems<std::index_sequence<Is...>, Args...>;
		using list = TL<Args...>;
		static constexpr size_t count = sizeof...(Args);

		template <size_t I>
		using get_cast = typename std::tuple_element<I, self>::type;

		template <size_t I>
		[[nodiscard]] friend constexpr auto get(AgTupElems & tup) noexcept -> get_cast<I> &
		{
			return static_cast<AgTupElem<get_cast<I>, I>&>(tup).value;
		}

		template <size_t I>
		[[nodiscard]] friend constexpr auto get(AgTupElems && tup) noexcept -> get_cast<I> &&
		{
			return std::move(static_cast<AgTupElem<get_cast<I>, I>&>(tup)).value;
		}

		template <size_t I>
		[[nodiscard]] friend constexpr auto get(AgTupElems const & tup) noexcept -> get_cast<I> const &
		{
			return static_cast<AgTupElem<get_cast<I>, I> const &>(tup).value;
		}

		template <typename T>
		[[nodiscard]] friend constexpr auto get(AgTupElems & tup) noexcept -> T &
		{
			return get<AgTupIndexOf<self, T>>(tup);
		}

		template <typename T>
		[[nodiscard]] friend constexpr auto get(AgTupElems && tup) noexcept -> T &&
		{
			return get<AgTupIndexOf<self, T>>(tup);
		}

		template <typename T>
		[[nodiscard]] friend constexpr auto get(AgTupElems const & tup) noexcept -> T const &
		{
			return get<AgTupIndexOf<self, T>>(tup);
		}

		constexpr void swap(AgTupElems & other) noexcept((noexcept(swap(std::declval<Args&>(), std::declval<Args&>())) and ...))
		{
			using std::get;
			(swap(get<Is>(*this), get<Is>(other)), ...);
		}
	private:
		template <std::size_t I>
		HZ_ALWAYS_INLINE static constexpr bool less_recursive(AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			auto & l = get<I>(lhs);
			auto & r = get<I>(rhs);
			if constexpr (I == (count - 1))
			{
				return l < r;
			}
			else
			{
				if (l < r) return true;
				if (r < l) return false;
				return less_recursive<I+1>(lhs, rhs);
			}
		}
	public:
		[[nodiscard]] friend constexpr bool operator<(AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			return less_recursive<0>(lhs, rhs);
		}
	
		[[nodiscard]] friend constexpr bool operator==(AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			using std::get;
			auto cmp = [](auto const & l, auto const & r) { return (l == r); };
			return ( cmp(get<Is>(lhs), get<Is>(rhs)) and ... );
		}

		[[nodiscard]] friend constexpr bool operator> (AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			return (rhs < lhs);
		}

		[[nodiscard]] friend constexpr bool operator<=(AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			return not (lhs > rhs);
		}

		[[nodiscard]] friend constexpr bool operator>=(AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			return not (lhs < rhs);
		}

		[[nodiscard]] friend constexpr bool operator!=(AgTupElems const & lhs, AgTupElems const & rhs) noexcept
		{
			return not (lhs == rhs);
		}
	};

	template <typename ... Args>
	AgTupElems(Args...)->AgTupElems<std::make_index_sequence<sizeof...(Args)>, Args...>;

	template <typename ...Args>
	void MatchAgTupElems(AgTupElems<Args...> const &){};

	template <typename T>
	concept TupElems = requires(T tup){ { MatchAgTupElems(tup) }; };

	template <typename ... Args>
	using FlatAgTup = AgTupElems<std::make_index_sequence<sizeof...(Args)>, Args...>;

	template <typename ... Args>
	struct AgTup : FlatAgTup<Args...>
	{
		using reference_type = AgTup<std::add_lvalue_reference_t<Args>...>;
		using const_reference_type = AgTup<std::add_lvalue_reference_t<std::add_const_t<Args>>...>;

		template <typename ... Ds>
			requires( requires(Args ... args) { AgTup<Ds...>{ args... }; } and not std::same_as<AgTup, AgTup<Ds...>>)
		[[nodiscard]] constexpr operator AgTup<Ds...>() & noexcept
		{
			return []<size_t ... I>(AgTup & tuple, std::index_sequence<I...>) noexcept -> AgTup<Ds...>
			{
				return { get<I>(tuple)... };
			}(*this, std::make_index_sequence<sizeof...(Args)>{});
		}

		template <typename ... Ds>
			requires(std::constructible_from<AgTup<Ds...>, Args...> and not std::same_as<AgTup<Ds...>, AgTup>)
		[[nodiscard]] constexpr operator AgTup<Ds...>() && noexcept
		{
			return []<size_t ... I>(auto && tuple, std::index_sequence<I...>) noexcept -> AgTup<Ds...>
			{
				return { std::move(get<I>(HZ_FWD(tuple)))... };
			}(*this, std::make_index_sequence<sizeof...(Args)>{});
		}

		template <typename ... Ds>
			requires( requires(std::add_const_t<Args> ... args) { AgTup<Ds...>{ args... }; } and not std::same_as<AgTup, AgTup<Ds...>>)
		[[nodiscard]] constexpr operator AgTup<Ds...>() const & noexcept
		{
			return []<size_t ... I>(AgTup const & tuple, std::index_sequence<I...>) noexcept -> AgTup<Ds...>
			{
				return { get<I>(tuple)... };
			}(*this, std::make_index_sequence<sizeof...(Args)>{});
		}

		template <typename ... Bs>
			requires (requires (Args & ... as, Bs const & ... bs){{ ((as = bs), ...) };})
		constexpr AgTup& operator=(AgTup<Bs...> const & rhs) noexcept
		{
			Apply
			(
				[](Args & ... args, Bs const & ... bs)
				{
					((args = bs), ...);
				},
				*this,
				rhs
			);
			return *this;
		}
	};

	template <typename ... Args>
	AgTup(Args...)->AgTup<Args...>;

	template <typename ... Args>
	[[nodiscard]] constexpr auto tie(Args & ... args) noexcept -> AgTup<Args...>
	{
		return { args ... };
	}

	template <typename ... Args>
	[[nodiscard]] constexpr auto make_tuple(Args && ... args) -> AgTup<std::decay_t<Args>...>
	{
		return { HZ_FWD(args) ... };
	}

	template <typename ... Args>
	[[nodiscard]] constexpr auto forward_as_tuple(Args && ... args) noexcept -> AgTup<Args&&...>
	{
		return { HZ_FWD(args) ... };
	}

	template <size_t I, size_t TupI, size_t O, typename ... More>
	struct TupleCatImpl
	{
		template <typename ... Tups>
		static constexpr decltype(auto) invoke(Tups && ... tups) noexcept
		{
			using std::get;
			return get<I>(get<TupI>(forward_as_tuple(HZ_FWD(tups)...)));
		}
	};

	template <size_t I, size_t TupI, size_t O, typename ... Args, typename ... More>
		requires (I >= (O + sizeof...(Args)))
	struct TupleCatImpl<I, TupI, O, TL<Args...>, More...>
		: TupleCatImpl<(I - sizeof...(Args)), (TupI + 1), (O + sizeof...(Args)), More...>
	{};

	template <size_t I, typename ... More>
	using TupleCatAt = TupleCatImpl<I, 0, 0, std::remove_cvref_t<More>...>;

	template <TupElems ... Tups>
	[[nodiscard]] constexpr auto tuple_cat(Tups && ... tuples) -> Hz::TLCombine<AgTup, std::remove_cvref_t<Tups>...>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) 
		{
			return AgTup
			{
				( TupleCatAt<Is, typename std::remove_cvref_t<Tups>::list...>::invoke( HZ_FWD(tuples) ... ) )
				...
			};
		}(std::make_index_sequence<(std::remove_cvref_t<Tups>::count + ...)>{});
	}

	template <typename F, TupElems Tuple>
	HZ_ALWAYS_INLINE constexpr decltype(auto) apply(F&& f, Tuple&& t)
	{
		return [&]<size_t ... I>(std::index_sequence<I...>) -> decltype(auto)
		{
			using std::get;
			return HZ_FWD(f)(get<I>(HZ_FWD(t))...);
		}
		(std::make_index_sequence<std::remove_reference_t<Tuple>::count>{});
	}

	template <size_t InsertIndex, typename Sub>
	struct AgTupInsertInvokable
	{
		Sub sub;

		template <size_t Is>
		static constexpr bool CanInsert = (Is == InsertIndex);

		template <size_t Is>
		static constexpr bool CanContinueBeforeInsert = (Is < InsertIndex);

		template <size_t Is>
		static constexpr bool CanContinueAfterInsert = (Is > InsertIndex);

		template <size_t Is, typename... Types> requires (CanInsert<Is>)
		constexpr auto Get(AgTup<Types...> && tuple) noexcept -> Sub&&
		{
			return std::move(sub);
		}

		template <size_t Is, typename... Types> requires (CanInsert<Is>)
		constexpr auto Get(AgTup<Types...> const & tuple) noexcept -> Sub&&
		{
			return std::move(sub);
		}

		template <size_t Is, typename... Types> requires (CanContinueBeforeInsert<Is>)
		static constexpr auto Get(AgTup<Types...> const & tuple) noexcept -> TLAtIndex<TL<Types...>, Is> const &
		{
			return get<Is>(tuple);
		}

		template <size_t Is, typename... Types> requires (CanContinueBeforeInsert<Is>)
		static constexpr auto Get(AgTup<Types...> && tuple) noexcept -> TLAtIndex<TL<Types...>, Is> &&
		{
			return std::move(get<Is>(tuple));
		}

		template <size_t Is, typename... Types> requires (CanContinueAfterInsert<Is>)
		static constexpr auto Get(AgTup<Types...> const & tuple) noexcept -> TLAtIndex<TL<Types...>, Is - 1> const & 
		{
			return get<Is - 1>(tuple);
		}

		template <size_t Is, typename... Types> requires (CanContinueAfterInsert<Is>)
		static constexpr auto Get(AgTup<Types...> && tuple) noexcept -> TLAtIndex<TL<Types...>, Is - 1> && 
		{
			return std::move(get<Is - 1>(tuple));
		}
	};

	template <size_t I, typename Sub>
	struct AgTupReplaceInvokable
	{
		Sub sub;

		template <size_t Is, typename... Types> requires (Is == I)
		constexpr auto Get(AgTup<Types...> const & tuple) noexcept -> Sub&&
		{
			return std::move(sub);
		}

		template <size_t Is, typename... Types> requires (Is != I)
		constexpr auto Get(AgTup<Types...> const & tuple) const noexcept -> TLAtIndex<TL<Types...>, Is> const &
		{
			return get<Is>(tuple);
		}

		template <size_t Is, typename... Types> requires (Is != I)
		constexpr auto Get(AgTup<Types...> && tuple) const noexcept -> TLAtIndex<TL<Types...>, Is> &&
		{
			return std::move(get<Is>(tuple));
		}
	};

	template <size_t I, typename Sub, typename... Types, typename ... Args>
	constexpr auto Insert(AgTup<Types...> &&tuple, Args && ... args) noexcept -> TLTranslate<TLInsertAt<TL<Types...>, I, Sub>, AgTup>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) -> TLTranslate<TLInsertAt<TL<Types...>, I, Sub>, AgTup>
		{
			auto v = AgTupInsertInvokable<I, Sub>{ HZ_FWD(args)... };
			return { std::move(v.template Get<Is>(std::move(tuple))) ... };
		}
		(std::make_index_sequence<sizeof...(Types) + 1>{});
	}

	template <size_t I, typename Sub, typename... Types, typename ... Args>
	constexpr auto Insert(AgTup<Types...> const &tuple, Args && ... args) noexcept -> TLTranslate<TLInsertAt<TL<Types...>, I, Sub>, AgTup>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) -> TLTranslate<TLInsertAt<TL<Types...>, I, Sub>, AgTup>
		{
			auto v = AgTupInsertInvokable<I, Sub>{ HZ_FWD(args)... };
			return { v.template Get<Is>(tuple) ... };
		}
		(std::make_index_sequence<sizeof...(Types) + 1>{});
	}

	template <size_t I, typename Sub, typename... Types, typename ... Args>
		requires (I < sizeof...(Types))
	constexpr auto Replace(AgTup<Types...> &&tuple, Args && ... args) noexcept -> TLTranslate<TLReplaceAt<TL<Types...>, I, Sub>, AgTup>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) -> TLTranslate<TLReplaceAt<TL<Types...>, I, Sub>, AgTup>
		{
			auto v = AgTupReplaceInvokable<I, Sub>{ HZ_FWD(args)... };
			return { std::move(v.template Get<Is>(std::move(tuple))) ... };
		}
		(std::make_index_sequence<sizeof...(Types)>{});
	}

	template <size_t I, typename Sub, typename... Types, typename ... Args>
		requires (I < sizeof...(Types))
	constexpr auto Replace(AgTup<Types...> const &tuple, Args && ... args) noexcept -> TLTranslate<TLReplaceAt<TL<Types...>, I, Sub>, AgTup>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) -> TLTranslate<TLReplaceAt<TL<Types...>, I, Sub>, AgTup>
		{
			auto v = AgTupReplaceInvokable<I, Sub>{ HZ_FWD(args)... };
			return { v.template Get<Is>(tuple) ... };
		}
		(std::make_index_sequence<sizeof...(Types)>{});
	}

	template <size_t I, typename... Args>
		requires (I < sizeof...(Args))
	constexpr auto Remove(AgTup<Args...> const &tuple) noexcept -> TLTranslate<TLRemoveAt<TL<Args...>, I>, AgTup>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) -> TLTranslate<TLRemoveAt<TL<Args...>, I>, AgTup>
		{
			using std::get;
			return { get<Is + (Is >= I)>(tuple)... };
		}
		(std::make_index_sequence<sizeof...(Args) - 1>{});
	}

	template <size_t I, typename... Args>
		requires (I < sizeof...(Args))
	constexpr auto Remove(AgTup<Args...> &&tuple) noexcept -> TLTranslate<TLRemoveAt<TL<Args...>, I>, AgTup>
	{
		return [&]<size_t ... Is>(std::index_sequence<Is...>) -> TLTranslate<TLRemoveAt<TL<Args...>, I>, AgTup>
		{
			using std::get;
			return { std::move(get<Is + (Is >= I)>(tuple))... };
		}
		(std::make_index_sequence<sizeof...(Args) - 1>{});
	}
}