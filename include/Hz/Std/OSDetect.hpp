#pragma once
#include <climits>
#include <string_view>
#include <cstddef>

namespace Hz
{
#if defined(_WIN32) || defined(_WIN64)
    #define PLATFORM_IS_WINDOWS
#elif __APPLE__
    #define PLATFORM_IS_MAC
#elif linux
    #define PLATFORM_IS_LINUX
#else
#ifdef EMSCRIPTEN 
	#define PLATFORM_IS_WASM
#else
	#error "Unsupported platform"
#endif
#endif

	enum class OS
	{
		Windows,
		Linux,
		Mac,
		Wasm,
		Unknown
	};

	inline constexpr std::size_t word_size = sizeof(void*) * CHAR_BIT;
	inline constexpr std::size_t half_word_size = word_size / 2;

#if defined(PLATFORM_IS_WINDOWS)
	inline constexpr OS target_os = OS::Windows;
	inline constexpr std::string_view new_line = "\r\n";
	inline constexpr std::string_view invalid_filename_chars = "\\/\x7C\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F";
	inline constexpr char directory_seperator_char = '\\';
	inline constexpr char alt_directory_seperator_char = '/';
	inline constexpr char path_seperator = ';';
	inline constexpr char volume_seperator_char = ':';
#elif defined(PLATFORM_IS_MAC)
	inline constexpr OS target_os = OS::Mac;
	inline constexpr std::string_view new_line = "\r";
	inline constexpr std::string_view invalid_filename_chars = "/\x00";
	inline constexpr char directory_seperator_char = '/';
	inline constexpr char alt_directory_seperator_char = '/';
	inline constexpr char path_seperator = ':';
	inline constexpr char volume_seperator_char = '/';
#elif defined(PLATFORM_IS_LINUX)
	inline constexpr OS target_os = OS::Linux;
	inline constexpr std::string_view new_line = "\n";
	inline constexpr std::string_view invalid_filename_chars = "/\x00";
	inline constexpr char directory_seperator_char = '/';
	inline constexpr char alt_directory_seperator_char = '/';
	inline constexpr char path_seperator = ':';
	inline constexpr char volume_seperator_char = '/';
#else
	inline constexpr OS target_os = OS::Unknown;
	inline constexpr std::string_view new_line = "\n";
	inline constexpr std::string_view invalid_filename_chars = "/\x00";
	inline constexpr char directory_seperator_char = '/';
	inline constexpr char alt_directory_seperator_char = '/';
	inline constexpr char path_seperator = ':';
	inline constexpr char volume_seperator_char = '/';
#endif
}