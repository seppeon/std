#pragma once
#include <Hz/TL/TLMerge.hpp>
#include <Hz/TL/TLCommon.hpp>
#include <Hz/TL/TTLCommon.hpp>

namespace Hz
{
    template <HZ_TTL_ARG ... Args>
    struct Mixer{};

    template <typename ... Args>
    struct MixerMergeImpl;
    template <HZ_TTL_ARG ... LhsArgs>
    struct MixerMergeImpl<Mixer<LhsArgs...>>
    {
        using type = Mixer<LhsArgs...>;
    };
    template <HZ_TTL_ARG ... LhsArgs, HZ_TTL_ARG ... RhsArgs>
    struct MixerMergeImpl<Mixer<LhsArgs...>, Mixer<RhsArgs...>>
    {
        using type = Mixer<LhsArgs..., RhsArgs...>;
    };
    template <HZ_TTL_ARG ... LhsArgs, HZ_TTL_ARG ... RhsArgs, typename ... Rest>
    struct MixerMergeImpl<Mixer<LhsArgs...>, Mixer<RhsArgs...>, Rest...>
    {
        using type = typename MixerMergeImpl<Mixer<LhsArgs..., RhsArgs...>, Rest...>::type;
    };
    
    template <typename ... Args>
    using MixerMerge = typename MixerMergeImpl<Args...>::type;

    template <HZ_TTL_ARG T>
    struct TemplateTemplate{};

    template <typename>
    struct MixerToTL;
    
    template <HZ_TTL_ARG ... Args>
    struct MixerToTL<Mixer<Args...>>
    {
        using type = TL<TemplateTemplate<Args>...>;
    };

    template <typename>
    struct TLToMixer;

    template <HZ_TTL_ARG ... Args>
    struct TLToMixer<TL<TemplateTemplate<Args>...>>
    {
        using type = Mixer<Args...>;
    };

    template <typename Mixy>
    struct MixerFilterImpl
    {
        using type = typename TLToMixer<typename TLRemoveDuplicates<typename MixerToTL<Mixy>::type>::type>::type;
    };

    template <typename Mixy>
    using MixerFilter = typename MixerFilterImpl<Mixy>::type;
}