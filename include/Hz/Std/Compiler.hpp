#pragma once
#include "Hz/Std/CompilerMacro.hpp"
#include "Hz/Std/OSDetect.hpp"
#include <string_view>
#include <cstdint>

namespace Hz
{
	enum class StdLibrary
	{
		LibCpp,
		LibStdCpp,
		MSVC,
		Unknown
	};

	namespace Impl
	{
		template<size_t N>
		constexpr unsigned ToUint( const char * str ) noexcept
		{
			while ( *str == ' ' ) ++str;
			unsigned output = 0;
			for ( size_t i = 0; i < N; ++i )
			{
				auto d = str[i];
				if ( '0' <= d and d <= '9' )
				{
					output *= 10;
					output += ( str[i] - '0' );
				}
				else { break; }
			}
			return output;
		}
	}

	inline constexpr uint8_t compiled_month = []() noexcept -> uint8_t
	{
		constexpr std::string_view month_lut[]{
			{ "Jan" },
			{ "Feb" },
			{ "Mar" },
			{ "Apr" },
			{ "May" },
			{ "Jun" },
			{ "Jul" },
			{ "Aug" },
			{ "Sep" },
			{ "Oct" },
			{ "Nov" },
			{ "Dec" },
		};
		for ( size_t i = 0; i < std::size( month_lut ); ++i )
		{
			if ( std::string_view{ __DATE__ }.find_first_of( month_lut[i] ) != std::string_view::npos ) { return static_cast<uint8_t>( i + 1 ); }
		}
		return 0;
	}();
	inline constexpr uint8_t compiled_day = static_cast<uint8_t>( Impl::ToUint<2>( &__DATE__[3 + 1] ) );
	inline constexpr uint16_t compiled_year = static_cast<uint16_t>( Impl::ToUint<4>( &__DATE__[3 + 1 + 3] ) );
	inline constexpr uint8_t compiled_hours = Impl::ToUint<2>( &__TIME__[0] );
	inline constexpr uint8_t compiled_minutes = Impl::ToUint<2>( &__TIME__[2 + 1] );
	inline constexpr uint8_t compiled_seconds = Impl::ToUint<2>( &__TIME__[2 + 1 + 2 + 1] );

#define STRINGIFY( x ) #x
#define TOSTRING( x ) STRINGIFY( x )
#ifdef _LIBCPP_VERSION
	inline constexpr StdLibrary compiled_std_library = StdLibrary::LibCpp;
#	define COMPILED_WITH_STDLIB_LIBCPP
#endif
#if defined( __GLIBCXX__ ) || defined( __GLIBCPP__ )
#	ifdef __GLIBCPP__
#		define GLIBVER __GLIBCPP__
#	else
#		define GLIBVER __GLIBCXX__
#	endif
	inline constexpr StdLibrary compiled_std_library = StdLibrary::LibStdCpp;
#	define COMPILED_WITH_STDLIB_LIBSTDCPP
#endif
#ifdef _MSC_VER
	inline constexpr StdLibrary compiled_std_library = StdLibrary::MSVC;
#	define COMPILED_WITH_STDLIB_MSVC
#endif
#undef STRINGIFY
#undef TOSTRING
}