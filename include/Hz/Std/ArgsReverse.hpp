#pragma once
#include <Hz/Traits/Fwd.hpp>
#include "Hz/Std/AlwaysInline.hpp"
#include "Hz/Std/ArgsAtIndex.hpp"
#include <cstddef>
#include <utility>

namespace Hz
{
	namespace Impl
	{
		template <std::size_t ... Is, typename F, typename ... Args>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr decltype(auto) ArgsReverse(std::index_sequence<Is...>, F&& fn, Args&& ... args)
		{
			return HZ_FWD(fn)(Hz::ArgsAtIndex<sizeof...(Args) - Is - 1>(HZ_FWD(args)...)...);
		}
	}

	template <typename F, typename ... Args>
	[[nodiscard]] HZ_ALWAYS_INLINE constexpr decltype(auto) ArgsReverse(F&& fn, Args&& ... args)
	{
		return Impl::ArgsReverse(std::make_index_sequence<sizeof...(Args)>{}, HZ_FWD(fn), HZ_FWD(args)...);
	}
}