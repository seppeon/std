#pragma once
#include "Hz/Std/CompilerMacro.hpp"

#if defined(COMPILED_WITH_MSVC)
#define ASSUME(cond) __assume(cond)
#elif defined(COMPILED_WITH_GNU) or defined(COMPILED_WITH_CLANG)
#define ASSUME(cond) do { if (not (cond)) __builtin_unreachable(); } while(false)
#else
// Fallback does nothing.
#define ASSUME(cond) do { if (not (cond)) __builtin_unreachable(); } while(false)
#endif