#pragma once
#include "Hz/Std/Ctor.hpp"
#include "Hz/Std/Fold.hpp"
#include "Hz/Std/Mixer.hpp"
#include <Hz/TL/TTLCommon.hpp>
#include <Hz/TL/TLCommon.hpp>
#include <Hz/Traits/Fwd.hpp>
#include <Hz/Traits/Value.hpp>

namespace Hz
{
    struct None{};

    template<typename Base, HZ_TTL_ARG ... Args>
    struct Mixin;

    template <typename Base, typename Mixy>
    struct ToMixinImpl;
    
    template <typename Base, HZ_TTL_ARG ... Args>
    struct ToMixinImpl<Base, Mixer<Args...>>
    {
        using type = Mixin<Base, Args...>;
    };

    template <typename Base, typename Mixy>
    using ToMixin = typename ToMixinImpl<Base, Mixy>::type;
    
    template <HZ_TTL_ARG Mixer, typename Mix>
    constexpr auto const & Get(Mix const & self) noexcept
    {
        using type = decltype(Mix::CastType(std::declval<Mixer<None>>()));
        return static_cast<type const &>(self);
    }

    template <HZ_TTL_ARG Mixer, typename Mix>
    constexpr auto & Get(Mix & self) noexcept
    {
        using type = decltype(Mix::CastType(std::declval<Mixer<None>>()));
        return static_cast<type &>(self);
    }

    template <typename Mix, std::size_t I>
    using MixElement = decltype(Mix::CastType(SizeTag<Mix::count - 1 - I>{}));

    template <std::size_t I, typename Mix>
    constexpr auto const & Cast(Mix const & self) noexcept
    {
        return static_cast<MixElement<Mix, I> const &>(self);
    }

    template <std::size_t I, typename Mix>
    constexpr auto & Cast(Mix & self) noexcept
    {
        return static_cast<MixElement<Mix, I> &>(self);
    }
    
    template <typename Mix>
    constexpr std::size_t Size() noexcept
    {
        return Mix::base::count;
    }

    template<typename Base>
    struct Mixin<Base>
    {
        using root = Base;

        static constexpr size_t index = -1;

        constexpr Base & Self() noexcept { return static_cast<Base&>(*this); }

        constexpr Base const & Self() const noexcept { return static_cast<Base const &>(*this); }

        static constexpr Mixin CastType( None ) noexcept;

        template <typename ... Args>
        static constexpr Mixin MakeMixin(Args &&...args) noexcept { return { HZ_FWD(args)... }; }
    };

    template<typename Base, HZ_TTL_ARG Top, HZ_TTL_ARG ... Others>
    struct Mixin<Base, Top, Others...> : Top<Mixin<Base, Others...>>
    {
        using base = Base;
        using mix = Mixin<Base, Others...>;
        using top = Top<mix>;
        using blank = Top<None>;
        using top::CastType;

        static constexpr size_t index = mix::index + 1;
        static constexpr size_t count = 1 + sizeof...(Others);

        static constexpr top CastType(blank) noexcept;
        static constexpr top CastType(SizeTag<index>) noexcept;

        template <typename ... TopArgs, typename ... OtherParams>
        static constexpr top ConstructBase( Ctor<TType<Top>, TopArgs...> && top_params, OtherParams && ... others) noexcept
        {
            return std::move(top_params).template ApplyLeading<top>( top::MakeMixin( std::move( others )... ) );
        }

        template <typename ... TopArgs, typename ... OtherParams>
        static constexpr Mixin MakeMixin( Ctor<TType<Top>, TopArgs...> && top_params, OtherParams && ... args) noexcept
        {
            return { ConstructBase( std::move(top_params), std::move(args)...) };
        }

        template <typename ... OtherParams>
        static constexpr Mixin MakeMixin( OtherParams && ... args) noexcept
        {
            return { ConstructBase( Ctor{ TType<Top>{} }, std::move(args) ... ) };
        }

        constexpr Mixin(top && b) noexcept : top{ std::move(b) } {}

        constexpr Mixin() = default;
        constexpr Mixin(Mixin &&) = default;
        constexpr Mixin(Mixin const &) = default;
        constexpr Mixin & operator=(Mixin &&) = default;
        constexpr Mixin & operator=(Mixin const &) = default;

        template <typename ... TopArgs, typename ... OtherParams>
        constexpr Mixin( Ctor<TType<Top>, TopArgs...> && top_params, OtherParams && ... others ) noexcept :
            top{ ConstructBase( std::move( top_params ), std::move( others ) ... )}
        {}

        template <typename ... OtherParams>
        constexpr Mixin( mix const & parent, OtherParams && ... others ) noexcept :
            top{ parent, std::move( others )... }
        {}

        template <typename ... OtherParams>
        constexpr Mixin( OtherParams && ... others ) noexcept :
            top{ ConstructBase( Ctor{ TType<Top>{} }, std::move( others ) ... )}
        {}
    private:
        template <HZ_TTL_ARG M, typename Mix, typename F>
        constexpr static decltype(auto) Invoke(M<Mix> & mix, F && func) noexcept
        {
            using blanked = M<None>;
            auto fn{ HZ_FWD(func) };
            using result = decltype(fn(mix));
            if constexpr (std::is_void_v<result>)
            {
                fn(mix);
                return None{};
            }
            else
            {
                return fn(mix);
            }
        }

        template <typename F, size_t ... I>
        constexpr auto ForEachImpl(F && func, std::index_sequence<I...>) noexcept
        {
            auto fn{ HZ_FWD(func) };
            return AgTup{Invoke(Cast<I>(this->Self()), fn) ...};
        }

        template <typename F, size_t ... I>
        constexpr auto FoldRImpl(F && func, std::index_sequence<I...>) noexcept
        {
            return Hz::FoldR
            (
                func,
                Cast<I>( this->Self() )...
            );
        }

        template <typename F, size_t ... I>
        constexpr auto FoldLImpl(F && func, std::index_sequence<I...>) noexcept
        {
            return Hz::FoldL
            (
                func,
                Cast<I>( this->Self() )...
            );
        }
    public:
        template <typename F>
        constexpr auto ForEach(F && func) noexcept
        {
            return ForEachImpl
            (
                HZ_FWD(func),
                std::make_index_sequence<Size<Base>()>()
            );
        }

        template <typename F>
        constexpr auto FoldR(F && func) noexcept
        {
            return FoldRImpl
            (
                HZ_FWD(func),
                std::make_index_sequence<Size<Base>>()
            );
        }

        template <typename F>
        constexpr auto FoldL(F && func) noexcept
        {
            return FoldLImpl
            (
                HZ_FWD(func),
                std::make_index_sequence<Size<Base>>()
            );
        }
    };
}