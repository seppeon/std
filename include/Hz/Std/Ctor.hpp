#pragma once
#include "Hz/Std/Apply.hpp"
#include <Hz/Traits/Fwd.hpp>
#include "Hz/Std/AgTup.hpp"
#include <Hz/TL/TLCommon.hpp>
#include <Hz/TL/TTLCommon.hpp>

namespace Hz
{
	template <typename Tg, typename ... FwdArgs>
	struct Ctor
	{
		constexpr Ctor( Tg, FwdArgs && ... args ) noexcept :
			m_params{ HZ_FWD(args) ... }
		{}

		template <typename C, typename ... LeadingArgs>
		[[nodiscard]] constexpr C ApplyLeading( LeadingArgs && ... args ) noexcept
		{
			return Apply([&]( auto && ... params ) noexcept -> C
			{
				return C{ HZ_FWD(args)..., HZ_FWD(params) ... };
			}, m_params);
		}

		template <typename C, typename ... TrailingArgs>
		[[nodiscard]] constexpr C ApplyTrailing( TrailingArgs && ... args ) noexcept
		{
			return Apply([&]( auto && ... params ) noexcept -> C
			{
				return { HZ_FWD(params)..., HZ_FWD( args ) ... };
			}, m_params);
		}
	private:
		AgTup<FwdArgs...> m_params;
	};

	template <template <typename> typename T, typename ... Args>
	Ctor(TType<T>, Args&&...)->Ctor<TType<T>, Args&&...>;
} 