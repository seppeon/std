#pragma once
#include "Hz/Std/AlwaysInline.hpp"
#include "Hz/Std/Union.hpp"
#include "Hz/Std/Integer.hpp"
#include <Hz/Traits/Fwd.hpp>
#include <Hz/Traits/Value.hpp>
#include <Hz/Traits/FnTraits.hpp>
#include <Hz/TL/TLAtIndex.hpp>
#include <Hz/TL/TLCartesianProduct.hpp>
#include <Hz/TL/TLAppend.hpp>
#include <Hz/TL/TLFilter.hpp>
#include <Hz/TL/TLTranslate.hpp>
#include <Hz/TL/TLUnique.hpp>
#include <type_traits>
#include <utility>
#include <variant>

namespace Hz
{
	template<typename F, typename List>
	concept VariantProducer = TLContainsRemoveCvref<GetFnResult<F>, List>;

	template<typename F, typename Arg>
	concept VariantInvokableWith = std::is_invocable_v<F, Arg>;

	template<typename F, typename... Args>
	using IsVariantConsumer = ValueTag<( VariantInvokableWith<F, Args> or ... )>;

	template<typename F, typename List>
	concept VariantConsumer = ( TLTranslate<TLAppend<TL<F>, List>, IsVariantConsumer>::value );

	template<typename F, typename R>
	struct VariantAutoConvert
	{
		F & function;
		// This uses the power of lemons to avoid extra copies.
		HZ_ALWAYS_INLINE constexpr operator R() const { return function(); }
	};

	template <typename T>
	struct Elem
	{
		using value_type = T;
		T value;
		HZ_ALWAYS_INLINE constexpr T operator ()() { return HZ_FWD(value); }
	};
	template <typename T>
	Elem(T&&)->Elem<T>;

	template<typename... Args> requires UniquePack<Args...>
	struct Variant;

	template<typename>
	inline constexpr bool IsVariantImpl = false;
	template<typename... Args> requires UniquePack<Args...>
	inline constexpr bool IsVariantImpl<Variant<Args...>> = true;
	template<typename T>
	concept IsVariant = IsVariantImpl<std::remove_cvref_t<T>>;
	template<typename T>
	concept IsNVariant = not IsVariantImpl<std::remove_cvref_t<T>>;

	template <typename F, IsVariant ... Vs>
	[[nodiscard]] HZ_ALWAYS_INLINE constexpr decltype(auto) Visit(F && fn, Vs && ... variant);

	template<typename... Args> requires UniquePack<Args...>
	struct Variant
	{
		using unique_list = TL<Args...>;
		static constexpr std::size_t count = sizeof...( Args );
	private:
		using union_elems = Union<Args...>;

		template<VariantProducer<unique_list> F>
		using producer_type = GetFnResult<F>;

		template<VariantConsumer<unique_list> F>
		using consumer_type = GetFirstArg<F>;

		static constexpr bool trivial_copy_ctor = ( std::is_trivially_copy_constructible_v<Args> and ... );
		static constexpr bool copy_ctor = ( std::is_copy_constructible_v<Args> and ... ) and not trivial_copy_ctor;
		static constexpr bool no_copy_ctor = not trivial_copy_ctor and not copy_ctor;

		static constexpr bool trivial_copy_assignment = ( std::is_trivially_copy_assignable_v<Args> and ... );
		static constexpr bool copy_assignment = ( std::is_copy_assignable_v<Args> and ... ) and not trivial_copy_assignment;
		static constexpr bool no_copy_assignment = not trivial_copy_assignment and not copy_assignment;

		static constexpr bool trivial_move_ctor = ( std::is_trivially_move_constructible_v<Args> and ... );
		static constexpr bool move_ctor = ( std::is_move_constructible_v<Args> and ... ) and not trivial_move_ctor;
		static constexpr bool no_move_ctor = not trivial_move_ctor and not move_ctor;

		static constexpr bool trivial_move_assignment = ( std::is_trivially_move_assignable_v<Args> and ... );
		static constexpr bool move_assignment = ( std::is_move_assignable_v<Args> and ... ) and not trivial_move_assignment;
		static constexpr bool no_move_assignment = not trivial_move_assignment and not move_assignment;

		static constexpr bool trivial_dtor = ( std::is_trivially_destructible_v<Args> and ... );
	public:
		constexpr Variant() = delete;

		// Construct allowing RVO :D
		template<VariantProducer<unique_list> F>
		HZ_ALWAYS_INLINE constexpr Variant( F producer )
		{
			Emplace( producer );
		}
		template <TLContains<unique_list> T, typename ... TArgs>
			requires std::constructible_from<T, TArgs...>
		HZ_ALWAYS_INLINE constexpr Variant( Type<T>, TArgs && ... args)
		{
			Emplace<T>( HZ_FWD(args)... );
		}

		// Deleted things :(
		constexpr Variant( Variant const & ) requires( no_copy_ctor ) = delete;
		constexpr Variant & operator=( Variant const & ) requires( no_copy_assignment ) = delete;
		constexpr Variant( Variant && ) requires( no_move_ctor ) = delete;
		constexpr Variant & operator=( Variant && ) & requires( no_move_assignment ) = delete;

		HZ_ALWAYS_INLINE constexpr Variant( Variant const & other ) requires( copy_ctor )
		{
			Hz::Visit(
				[this]<typename T>( T && value ) { this->Emplace( Elem{ value }); },
				other
			);
		}
		HZ_ALWAYS_INLINE constexpr Variant & operator=( Variant const & other ) requires( copy_assignment )
		{
			Hz::Visit(
				[this]<typename T>( T && value ) { this->Emplace( Elem{ value }); },
				other
			);
			return *this;
		}
		HZ_ALWAYS_INLINE constexpr Variant( Variant && other ) requires( move_ctor )
		{
			Hz::Visit(
				[this]<typename T>( T && value ) { this->Emplace( Elem{ static_cast<T &&>( value ) } ); },
				other
			);
			other.Destroy();
		}
		HZ_ALWAYS_INLINE constexpr Variant & operator=( Variant && other ) & requires( move_assignment )
		{
			Hz::Visit(
				[this]<typename T>( T && value )
				{
					this->Emplace(Elem{ static_cast<T &&>( value ) });
				},
				other
			);
			other.Destroy();
			return *this;
		}

		// Trivial thingos :)
		constexpr Variant( Variant const & ) requires( trivial_copy_ctor ) = default;
		constexpr Variant & operator=( Variant const & ) requires( trivial_copy_assignment ) = default;
		constexpr Variant( Variant && ) requires( trivial_move_ctor ) = default;
		constexpr Variant & operator=( Variant && ) & requires( trivial_move_assignment ) = default;

		constexpr ~Variant() requires( trivial_dtor ) = default;
		constexpr ~Variant() requires( not trivial_dtor )
		{
			Destroy();
		}

		template <typename T>
		static constexpr std::size_t index_of_type = TLIndexOf<unique_list, T>;

		template <VariantProducer<unique_list> F>
		constexpr auto Emplace( F producer ) -> producer_type<F> &
		{
			Destroy();
			auto & output = m_elems.Construct( Type<std::remove_cvref_t<producer_type<F>>>{}, VariantAutoConvert<F, producer_type<F>>{ producer } );
			m_index = TLIndexOf<unique_list, std::remove_cvref_t<producer_type<F>>>;
			return output;
		}
		template <TLContains<unique_list> T, typename ... TArgs>
			requires (requires (TArgs && ... targs) { { T{HZ_FWD(targs)...} }; })
		constexpr auto Emplace(Type<T>, TArgs && ... args) -> T &
		{
			return Emplace([&]() -> T { return T{ std::forward<TArgs>(args)... }; });
		}

		[[nodiscard]] HZ_ALWAYS_INLINE constexpr std::size_t GetIndex() const noexcept
		{
			return static_cast<std::size_t>(m_index);
		}

		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool HasType(Variant const & variant) noexcept
		{
			return index_of_type<T> == variant.GetIndex();
		}

		[[nodiscard]] HZ_ALWAYS_INLINE constexpr bool HasValue() const noexcept
		{
			return m_index != count;
		}

		[[nodiscard]] HZ_ALWAYS_INLINE constexpr bool ValuelessByException() const noexcept
		{
			return m_index == count;
		}

		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator==( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return Compare<std::equal_to{}>( lhs, rhs );
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator==( Variant const & lhs, T const & rhs ) noexcept
		{
			return Compare<T, std::equal_to{}>( lhs, rhs );
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator==( T const & lhs, Variant const & rhs ) noexcept
		{
			return rhs == lhs;
		}
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator!=( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return not (lhs == rhs);
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator!=( T const & lhs, Variant const & rhs ) noexcept
		{
			return not (lhs == rhs);
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator!=( Variant const & lhs, T const & rhs ) noexcept
		{
			return not (lhs == rhs);
		}

		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator<( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return Compare<std::less{}>( lhs, rhs );
		}
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator>( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return Compare<std::greater{}>( lhs, rhs );
		}
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator<=( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return Compare<std::less_equal{}>( lhs, rhs );
		}
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator>=( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return Compare<std::greater_equal{}>( lhs, rhs );
		}

		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator<( Variant const & lhs, T const & rhs ) noexcept
		{
			return Compare<T, std::less{}>( lhs, rhs );
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator>( Variant const & lhs, T const & rhs ) noexcept
		{
			return Compare<T, std::greater{}>( lhs, rhs );
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator<=( Variant const & lhs, T const & rhs ) noexcept
		{
			return Compare<T, std::less_equal{}>( lhs, rhs );
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator>=( Variant const & lhs, T const & rhs ) noexcept
		{
			return Compare<T, std::greater_equal{}>( lhs, rhs );
		}

		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator<( T const & lhs, Variant const & rhs ) noexcept
		{
			return rhs > lhs;
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator>( T const & lhs, Variant const & rhs ) noexcept
		{
			return rhs < lhs;
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator<=( T const & lhs, Variant const & rhs ) noexcept
		{
			return rhs >= lhs;
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr bool operator>=( T const & lhs, Variant const & rhs ) noexcept
		{
			return rhs <= lhs;
		}

		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr auto UnsafeGet(Variant & variant) noexcept -> T &
		{
			return variant.m_elems.template Get<T>();
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr auto UnsafeGet(Variant && variant) noexcept -> T &&
		{
			return std::move(variant.m_elems.template Get<T>());
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr auto UnsafeGet(Variant const & variant) noexcept -> T const &
		{
			return variant.m_elems.template Get<T>();
		}

		// For compatibility with standard library its got safe access so its fine.
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr auto get_if(Variant * value) noexcept -> T *
		{
			return HasType<T>(*value) ? std::addressof(value->m_elems.template Get<T>()) : nullptr;
		}
		template <TLContains<unique_list> T>
		[[nodiscard]] HZ_ALWAYS_INLINE friend constexpr auto get_if(Variant const * value) noexcept -> T const *
		{
			return HasType<T>(*value) ? std::addressof(value->m_elems.template Get<T>()) : nullptr;
		}
	private:
		template<auto fn, typename T>
		struct CompareFnLhs
		{
			T const & lhs_elem;
			HZ_ALWAYS_INLINE constexpr bool operator()( T const & rhs_elem ) const noexcept
			{
				return fn( lhs_elem, rhs_elem );
			}
		};
		template<auto fn, typename T>
		struct CompareFnRhs
		{
			T const & rhs_elem;
			HZ_ALWAYS_INLINE constexpr bool operator()( T const & lhs_elem ) const noexcept
			{
				return fn( lhs_elem, rhs_elem );
			}
		};
		template<auto fn>
		struct CompareTopFn
		{
			Variant const & rhs;
			template<typename T>
			HZ_ALWAYS_INLINE constexpr bool operator()( T const & lhs_elem ) const noexcept
			{
				return Hz::Visit( CompareFnLhs<fn, T>{ lhs_elem }, rhs );
			}
		};

		template<auto op>
		HZ_ALWAYS_INLINE friend constexpr bool Compare( Variant const & lhs, Variant const & rhs ) noexcept
		{
			return ( lhs.m_index == rhs.m_index ) and Hz::Visit( CompareTopFn<op>{ rhs }, lhs );
		}
	
		template<TLContains<unique_list> T, auto op>
		HZ_ALWAYS_INLINE friend constexpr bool Compare( Variant const & lhs, T const & rhs ) noexcept
		{
			return ( lhs.m_index == index_of_type<T> ) and Hz::Visit( CompareFnRhs<op, T>{ rhs }, lhs );
		}

		struct DestroyImpl
		{
			HZ_ALWAYS_INLINE constexpr void operator()( auto && obj ) const noexcept
			{
				std::destroy_at( std::addressof( obj ) );
			}
		};

		HZ_ALWAYS_INLINE constexpr void Destroy() noexcept
		{
			if ( m_index != count )
			{
				Hz::Visit
				(
					DestroyImpl{},
					*this
				);
				m_index = count;
			}
		}

		template<typename F, typename D, typename Var>
		friend class VisitImpl;
	
		template<typename F>
		friend struct MultiVisitImpl;

		union_elems m_elems;
		UnsignedFittingValue<count> m_index = count;
	};

	namespace Impl
	{
		template <typename T>
		struct VisitAlternative;
		template <typename ... Ts, typename ... More>
		struct VisitAlternative<TL<TL<Ts...>, More...>>
		{
			template <typename F, IsVariant ... Vs> requires (sizeof...(Vs) == sizeof...(Ts))
			HZ_ALWAYS_INLINE static constexpr decltype(auto) invoke(F && fn, Vs && ... variant)
			{
				if ((HasType<Ts>(variant) & ...)) {
					return HZ_FWD(fn)(UnsafeGet<Ts>(variant)...);
				}
				if constexpr (sizeof...(More) == 0) {
					throw std::bad_variant_access();
				}
				else {
					return VisitAlternative<TL<More...>>::invoke(HZ_FWD(fn), HZ_FWD(variant)...);
				}
			}

			template <typename F, IsNVariant D, IsVariant ... Vs> requires (sizeof...(Vs) == sizeof...(Ts))
			HZ_ALWAYS_INLINE static constexpr decltype(auto) invokef(F && fn, D && fallback, Vs && ... variant)
			{
				if ((HasType<Ts>(variant) & ...)) {
					return HZ_FWD(fn)(UnsafeGet<Ts>(variant)...);
				}
				if constexpr (sizeof...(More) == 0) {
					return fallback();
				}
				else {
					return VisitAlternative<TL<More...>>::invokef(HZ_FWD(fn), HZ_FWD(fallback), HZ_FWD(variant)...);
				}
			}
		};
	}

	template <typename F, IsVariant ... Vs>
	[[nodiscard]] constexpr decltype(auto) Visit(F && fn, Vs && ... variant)
	{
		if ((variant.ValuelessByException() | ...))
		{
			throw std::bad_variant_access();
		}
		using type_lists = Hz::TLInvokableCartesianProduct<F, std::remove_cvref_t<Vs>...>;
		return Impl::VisitAlternative<type_lists>::invoke(HZ_FWD(fn), HZ_FWD(variant)...);
	}

	template <typename F, IsNVariant D, IsVariant ... Vs>
	[[nodiscard]] constexpr decltype(auto) Visit(F && fn, D && fallback, Vs && ... variant)
	{
		if ((variant.ValuelessByException() | ...))
		{
			throw std::bad_variant_access();
		}
		using type_lists = Hz::TLInvokableCartesianProduct<F, std::remove_cvref_t<Vs>...>;
		return Impl::VisitAlternative<type_lists>::invokef(HZ_FWD(fn), HZ_FWD(fallback), HZ_FWD(variant)...);
	}
}

#include <Hz/Traits/RegType.hpp>

namespace Hz
{
	template<class... Types>
	struct RegVariant<::Hz::Variant<Types...>> : std::true_type
	{
	};
}