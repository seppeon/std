#pragma once
#include <Hz/Std/Data.hpp>
#include <Hz/Std/Size.hpp>

namespace Hz
{
	/**
	 * @brief Checks if some type is both of a known size and `data(obj)` is avaliable.
	 * 
	 * @tparam Obj The object type to check.
	 */
	template <typename Obj>
	concept HasStaticallyKnownSizeAndData = IsNStaticallyKnownSize<Obj> and SupportsData<Obj>;
	static_assert( HasStaticallyKnownSizeAndData<const int ( & )[2]> );
	/**
	 * @brief Checks if some type is both of a known size and the `data(obj)` is avaliable
	 * 
	 * @tparam Obj The object type to check.
	 */
	template <typename Obj>
	concept HasSizeAndData = SupportsSize<Obj> and SupportsData<Obj>;
	/**
	 * @brief Checks if some type is both of a known size and the `data(obj)` method returns `T`.
	 * 
	 * @tparam Obj The object type to check.
	 * @tparam T The type of the result of `data(obj)`.
	 */
	template <typename Obj, typename T>
	concept HasSizeAndPointsTo = HasSizeAndData<Obj> and DatableTo<Obj, T>;
}