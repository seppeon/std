#pragma once
#include "Hz/Std/CompilerMacro.hpp"
#include <string_view>
#include <algorithm>

namespace Hz
{
	namespace Impl
	{
		template <typename T>
		[[nodiscard]] constexpr auto RawTypeName() -> const char *
		{
		#if defined(COMPILED_WITH_CLANG)
		#define FN_NAME __PRETTY_FUNCTION__
		#endif
		#if defined(COMPILED_WITH_MSVC)
		#define FN_NAME __FUNCSIG__  
		#endif
		#if defined(COMPILED_WITH_GNU)
		#define FN_NAME __PRETTY_FUNCTION__ 
		#endif
			return FN_NAME;
		#undef FN_NAME
		}
	}

	template <typename T>
	[[nodiscard]] consteval auto GetTypeName() -> std::string_view
	{
		struct parts
		{
			std::string_view prefix{};
			std::string_view suffix{};
			std::string_view function{};
		};
		auto [prefix, suffix, function] = [](std::string_view name) -> parts
		{
			switch (compiler)
			{
			case Compiler::Microsoft: 	return { "GetTypeName<", 	">(void)", 	name };
			case Compiler::Clang: 		return { "T = ", 			"]", 		name };
			case Compiler::GNU: 		return { "T = ", 	    	"]", 		name };
			}
			return {};
		}(Impl::RawTypeName<T>());
		auto beg = std::search(function.begin(), function.end(), prefix.begin(), prefix.end()) + prefix.size();
		auto end = (std::search(function.rbegin(), function.rend(), suffix.rbegin(), suffix.rend()) + suffix.size()).base();
		return std::string_view{ beg, end };
	}
}