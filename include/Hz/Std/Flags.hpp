#pragma once
#include "Hz/Std/UnderlyingValue.hpp"

namespace Hz
{
	template <typename T> requires std::is_enum_v<T>
	struct Flags
	{
		using value_type = std::underlying_type_t<T>;

		template <typename ... Inputs> requires (std::is_convertible_v<Inputs, T> and ...)
		constexpr Flags(Inputs const & ... inputs) noexcept
			: m_value{ (value_type{} | ... | UnderlyingValue(inputs)) }
		{}

		constexpr Flags(Flags &&) noexcept = default;
		constexpr Flags(Flags const &) noexcept = default;
		constexpr Flags& operator=(Flags &&) noexcept = default;
		constexpr Flags& operator=(Flags const &) noexcept = default;
		constexpr ~Flags() noexcept = default;

		template <typename V>
		[[nodiscard]] friend constexpr auto UnderlyingValue(Flags<V> const & input) noexcept -> typename Flags<V>::value_type
		{
			return input.m_value;
		}

		template <typename ... Inputs> requires (std::is_convertible_v<Inputs, T> and ...)
		[[nodiscard]] constexpr Flags Set(Inputs const & ... inputs) const noexcept
		{
			return { static_cast<T>((m_value | ... | UnderlyingValue(inputs))) };
		}

		template <typename ... Inputs> requires (std::is_convertible_v<Inputs, T> and ...)
		[[nodiscard]] constexpr Flags SetIf(bool condition, Inputs const & ... inputs) const noexcept
		{
			return condition ? Set(inputs...) : (*this);
		}

		template <typename ... Inputs> requires (std::is_convertible_v<Inputs, T> and ...)
		[[nodiscard]] constexpr Flags Clear(Inputs const & ... inputs) const noexcept
		{
			return { static_cast<T>(m_value & ~UnderlingValue(Flags(inputs ...))) };
		}

		template <typename ... Inputs> requires (std::is_convertible_v<Inputs, T> and ...)
		[[nodiscard]] constexpr Flags ClearIf(bool condition, Inputs const & ... inputs) const noexcept
		{
			return condition ? Clear(inputs...) : (*this);
		}

		[[nodiscard]] constexpr bool Check(T const input) const noexcept
		{
			return static_cast<T>(UnderlyingValue(input) & m_value) == input;
		}
	private:
		value_type m_value;
	};

	template <typename F, typename ... Inputs>
	Flags(F, Inputs ...) -> Flags<F>;

	template <typename T>
	[[nodiscard]] constexpr auto NegateUnderlyingValue(Flags<T> const & value) noexcept -> Flags<T>
	{
		return { static_cast<T>(-UnderlyingValue(value)) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto AbsUnderlyingValue(Flags<T> const & value) noexcept -> Flags<T>
	{
		auto v = UnderlyingValue(value);
		return { static_cast<T>(v < 0 ? -v : v) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto IncUnderlyingValue(Flags<T> const & value) noexcept -> Flags<T>
	{
		return { static_cast<T>(UnderlyingValue(value) + 1) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto DecUnderlyingValue(Flags<T> const & value) noexcept -> Flags<T>
	{
		return { static_cast<T>(UnderlyingValue(value) - 1) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto operator | (Flags<T> const & lhs, T const & rhs) noexcept -> Flags<T>
	{
		return { static_cast<T>( UnderlyingValue(lhs) | UnderlyingValue(rhs) ) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto operator | (T const & lhs, Flags<T> const & rhs) noexcept -> Flags<T>
	{
		return { static_cast<T>( UnderlyingValue(lhs) | UnderlyingValue(rhs) ) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto operator | (Flags<T> const & lhs, Flags<T> const & rhs) noexcept -> Flags<T>
	{
		return { static_cast<T>( UnderlyingValue(lhs) | UnderlyingValue(rhs) ) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto operator ^ (Flags<T> const & lhs, T const & rhs) noexcept -> Flags<T>
	{
		return { static_cast<T>( UnderlyingValue(lhs) ^ UnderlyingValue(rhs) ) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto operator ^ (T const & lhs, Flags<T> const & rhs) noexcept -> Flags<T>
	{
		return { static_cast<T>( UnderlyingValue(lhs) ^ UnderlyingValue(rhs) ) };
	}

	template <typename T>
	[[nodiscard]] constexpr auto operator ^ (Flags<T> const & lhs, Flags<T> const & rhs) noexcept -> Flags<T>
	{
		return { static_cast<T>( UnderlyingValue(lhs) ^ UnderlyingValue(rhs) ) };
	}
}