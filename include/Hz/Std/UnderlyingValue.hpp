#pragma once
#include <Hz/Traits/ConceptsStd.hpp>

namespace Hz
{
	template <Enum T>
	[[nodiscard]] constexpr auto UnderlyingValue(T const & input) noexcept -> std::underlying_type_t<T>
	{
		return static_cast<std::underlying_type_t<T>>( input );
	}
	template <Integer T>
	[[nodiscard]] constexpr auto UnderlyingValue(T const & input) noexcept -> T
	{
		return input;
	}

	template <typename T>
	[[nodiscard]] constexpr auto NegateUnderlyingValue(T const & value) noexcept -> T
	{
		return static_cast<T>(-UnderlyingValue(value));
	}

	template <typename T>
	[[nodiscard]] constexpr auto AbsUnderlyingValue(T const & value) noexcept -> T
	{
		auto v = UnderlyingValue(value);
		return static_cast<T>((v > 0) ? v : -v);
	}

	template <typename T>
	[[nodiscard]] constexpr auto IncUnderlyingValue(T const & value) noexcept -> T
	{
		return static_cast<T>(UnderlyingValue(value) + 1);
	}

	template <typename T>
	[[nodiscard]] constexpr auto DecUnderlyingValue(T const & value) noexcept -> T
	{
		return static_cast<T>(UnderlyingValue(value) - 1);
	}

	template <typename T>
	[[nodiscard]] constexpr auto IncUnderlyingValue(T const & value, T const & inc) noexcept -> T
	{
		return static_cast<T>(UnderlyingValue(value) + UnderlyingValue(inc));
	}

	template <typename T>
	[[nodiscard]] constexpr auto DecUnderlyingValue(T const & value, T const & dec) noexcept -> T
	{
		return static_cast<T>(UnderlyingValue(value) - UnderlyingValue(dec));
	}
}