#pragma once
#include "Hz/Std/UnionElems.hpp"
#include "Hz/Std/Lifetime.hpp"
#include "Hz/Std/AlwaysInline.hpp"
#include <Hz/TL/TLContains.hpp>
#include <Hz/TL/TLIndexOf.hpp>
#include <Hz/TL/TLUnique.hpp>
#include <Hz/Traits/FirstConstructible.hpp>

namespace Hz
{
	template <typename ... Fields> requires (UniquePack<Fields...>)
	class Union
	{
	private:
		using list = TL<Fields...>;

		UnionElems<0, Fields...> m_elems;
	public:
		template <TLContains<list> T, typename ... Args>
		HZ_ALWAYS_INLINE constexpr auto Construct(Type<T>, Args && ... args) noexcept(std::is_nothrow_constructible_v<T, Args &&...>) -> T &
		{
			return *ConstructAt<T>(GetUnionElemCtor<TLIndexOf<list, T>>(&m_elems), HZ_FWD(args)...);
		}

		template <TLContains<list> T, typename ... Args>
		HZ_ALWAYS_INLINE constexpr auto Construct(Args && ... args) noexcept(std::is_nothrow_constructible_v<T, Args &&...>) -> T &
		{
			return *ConstructAt<T>(GetUnionElemCtor<TLIndexOf<list, T>>(&m_elems), HZ_FWD(args)...);
		}

		template <TLContainsConvertible<list> T>
		HZ_ALWAYS_INLINE constexpr void Destruct() noexcept(std::is_nothrow_destructible_v<T>)
		{
			GetUnionElemDtor<TLIndexOf<list, T>>(&m_elems);
		}

		template <typename T>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr T * GetPtr() noexcept
		{
			return GetUnionElemAccess<TLIndexOf<list, T>>(&m_elems);
		}

		template <typename T>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr T const * GetPtr() const noexcept
		{
			return GetUnionElemAccess<TLIndexOf<list, T>>(&m_elems);
		}

		template <typename T>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr T & Get() & noexcept
		{
			return *GetPtr<T>();
		}

		template <typename T>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr T && Get() && noexcept
		{
			return std::move(*GetPtr<T>());
		}

		template <typename T>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr T const & Get() const & noexcept
		{
			return *GetPtr<T>();
		}

		constexpr Union() {}
		constexpr ~Union() {}
	};

	template <typename>
	inline constexpr bool is_hz_union_v = false;
	template <typename ... Fields>
	inline constexpr bool is_hz_union_v<Union<Fields...>> = true;

	template <typename T>
	concept HzUnion = is_hz_union_v<std::remove_cvref_t<T>>;
}