#pragma once
#include <string_view>

namespace Hz
{
	enum class Compiler
	{
		Microsoft,
		Clang,
		GNU,
		Unknown
	};

#if defined(__clang__)
	#define COMPILED_WITH_CLANG
	inline constexpr Compiler compiler = Compiler::Clang;
#elif defined(_MSC_VER)
	#define COMPILED_WITH_MSVC
	inline constexpr Compiler compiler = Compiler::Microsoft;
#elif defined(__GNUC__)
	#define COMPILED_WITH_GNU
	inline constexpr Compiler compiler = Compiler::GNU;
#else 
	#define COMPILED_WITH_UNKNOWN
	inline constexpr Compiler compiler = Compiler::Unknown;
#endif

	[[nodiscard]] constexpr std::string_view ToString(Compiler compiler) noexcept
	{
		switch (compiler){
		case Compiler::Microsoft: return "Microsoft";
		case Compiler::Clang: return "Clang";
		case Compiler::GNU: return "GNU";
		default: return "Unknown";
		}
	}

	[[nodiscard]] constexpr std::string_view GetCompilerName() noexcept
	{
		return ToString(compiler);
	}
}