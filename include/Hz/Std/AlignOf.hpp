#pragma once

namespace Hz
{
	template <typename T>
	[[nodiscard]] constexpr decltype(sizeof(char)) align_of() noexcept
	{
		return alignof(T);
	}
}