#pragma once
#include <Hz/Traits/Fwd.hpp>
#include <Hz/Traits/ConceptsStdLib.hpp>
#include <Hz/Traits/ConceptsCntLib.hpp>
#include <cstddef>

namespace Hz
{
	/**
	 * @brief A customisation pointer, providing the length of some statically sized container.
	 * 
	 * @note This is a customisation point for the user.
	 *
	 * @tparam T The type of the container, specialize on this type.
	 */
	template <typename T>
	struct NStaticallyKnownSize;
	/**
	 * @brief A specialization for ordinary arrays.
	 * 
	 * @tparam T The type of the elements in the array.
	 * @tparam N The length of the array.
	 */
	template <typename T, std::size_t N>
	struct NStaticallyKnownSize<T[N]>
	{
		static constexpr auto value = N; ///< The length of the container.
	};
	/**
	 * @brief A specialization for ordinary arrays.
	 * 
	 * @tparam T The type of the elements in the array.
	 * @tparam N The length of the array.
	 */
	template <typename T, std::size_t N>
	struct NStaticallyKnownSize<T(*)[N]>
	{
		static constexpr auto value = N; ///< The length of the container.
	};
	/**
	 * @brief A specialization for c++ arrays.
	 * 
	 * @tparam T The type of the elements in the array.
	 * @tparam N The length of the array.
	 */
	template <typename T, std::size_t N>
	struct NStaticallyKnownSize<std::array<T, N>>
	{
		static constexpr auto value = N; ///< The length of the container.
	};
	/**
	 * @brief Check if a type is registered as a statically known size.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept IsNStaticallyKnownSize = requires
	{
		{ NStaticallyKnownSize<std::remove_cvref_t<T>>::value } -> std::convertible_to<std::size_t>;
	};
	/**
	 * @brief Get the size of some fixed known size object.
	 * 
	 * @tparam T The type to check.
	 */
	template <IsNStaticallyKnownSize T>
	inline constexpr auto statically_known_size_v = NStaticallyKnownSize<std::remove_cvref_t<T>>::value;

	template <typename C>
		requires (not Ptr<C>)
	[[nodiscard]] constexpr size_t Size(C && c) noexcept
	{
		if constexpr (Cnt::HasSize<C&>)
		{
			return c.Size();
		}
		else if constexpr (Std::HasSize<C&>)
		{
			using std::size;
			return size( c );
		}
		else if constexpr (IsNStaticallyKnownSize<C>)
		{
			return NStaticallyKnownSize<std::remove_cvref_t<C>>::value;
		}
		else
		{
			return 1;
		}
	}

	template <Cnt::HasEmpty C>
	[[nodiscard]] constexpr bool Empty(C && c) noexcept
	{
		return c.Empty();
	}

	template <Hz::Std::HasEmpty C>
		requires (not Cnt::HasEmpty<C>)
	[[nodiscard]] constexpr bool Empty(C && c) noexcept
	{
		using std::empty;
		return empty( c );
	}

    template <typename T>
    concept Sizable = requires(T const & obj)
    {
        { ::Hz::Size(obj) } -> std::same_as<size_t>;
    };

	template<typename T>
	concept SupportsSize = ( Std::HasSize<T> or Cnt::HasSize<T> );
}