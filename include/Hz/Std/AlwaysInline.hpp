#pragma once
#include "Hz/Std/CompilerMacro.hpp"

#if defined(COMPILED_WITH_MSVC)
#define HZ_ALWAYS_INLINE [[msvc::forceinline]] inline
#elif defined(COMPILED_WITH_GNU) or defined(COMPILED_WITH_CLANG)
#define HZ_ALWAYS_INLINE [[gnu::always_inline]] inline
#else
// Fallback does nothing.
#define HZ_ALWAYS_INLINE inline
#endif