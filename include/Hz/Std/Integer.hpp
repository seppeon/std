#pragma once
#include <cstdint>
#include <cstddef>
#include <type_traits>
#include <bit>

namespace Hz
{	
	enum class IntegerType
	{
		Signed,
		Unsigned
	};

	extern void break_round_up_bits_constexpr();

	template <typename T>
	[[nodiscard]] constexpr auto RoundUpBits(T value) -> T
	{
		if (value <= 8)  return 8;
		if (value <= 16) return 16;
		if (value <= 32) return 32;
		if (value <= 64) return 64;
		break_round_up_bits_constexpr();
		return 0;
	}

	using f32 = float;
	using f64 = double;
	using u8 = std::uint8_t;
	using u16 = std::uint16_t;
	using u32 = std::uint32_t;
	using u64 = std::uint64_t;
	using uptr = std::uintptr_t; 
	using i8 = std::int8_t;
	using i16 = std::int16_t;
	using i32 = std::int32_t;
	using i64 = std::int64_t;
	using iptr = std::intptr_t; 
	using sz = std::size_t;
	using ssz = std::make_signed_t<sz>;
	using iptr = ::intptr_t;
	using uptr = std::uintptr_t;
	using dptr = std::ptrdiff_t;

	template<sz Bits, IntegerType Type>
	[[nodiscard]] constexpr auto FindIntegerType() noexcept
	{
		constexpr auto bits = RoundUpBits(Bits); 
		if constexpr (Type == IntegerType::Signed)
		{
			if constexpr (bits == 8)  return i8{};
			if constexpr (bits == 16) return i16{};
			if constexpr (bits == 32) return i32{};
			if constexpr (bits == 64) return i64{};
		}

		if constexpr (Type == IntegerType::Unsigned)
		{
			if constexpr (bits == 8)  return u8{};
			if constexpr (bits == 16) return u16{};
			if constexpr (bits == 32) return u32{};
			if constexpr (bits == 64) return u64{};
		}
	}

	template <sz Bits, IntegerType Type>
	using IntegerOfMinimumBits = decltype(FindIntegerType<Bits, Type>());

	template <std::uintmax_t value>
	using UnsignedFittingValue = IntegerOfMinimumBits<std::bit_width(value), IntegerType::Unsigned>;

	template <std::intmax_t value>
	using SignedFittingValue = IntegerOfMinimumBits<std::bit_width(std::uintmax_t((value < 0) ? -value : value)) + 1, IntegerType::Signed>;

	template <sz Bits>
	using UnsignedOfMinimumBits = IntegerOfMinimumBits<Bits, IntegerType::Unsigned>;

	template <sz Bits>
	using SignedOfMinimumBits = IntegerOfMinimumBits<Bits, IntegerType::Signed>;

	template <typename T>
	using MakeSigned = SignedOfMinimumBits<8u * sizeof( T )>;

	template <typename T>
	using MakeUnsigned = UnsignedOfMinimumBits<8u * sizeof( T )>;

	inline namespace IntegerLiterals
	{
		[[nodiscard]] constexpr auto operator""_u8(unsigned long long value) noexcept -> u8 { return static_cast<u8>(value); }
		[[nodiscard]] constexpr auto operator""_u16(unsigned long long value) noexcept -> u16 { return static_cast<u16>(value); }
		[[nodiscard]] constexpr auto operator""_u32(unsigned long long value) noexcept -> u32 { return static_cast<u32>(value); }
		[[nodiscard]] constexpr auto operator""_u64(unsigned long long value) noexcept -> u64 { return static_cast<u64>(value); }
		[[nodiscard]] constexpr auto operator""_i8(unsigned long long value) noexcept -> i8 { return static_cast<i8>(value); }
		[[nodiscard]] constexpr auto operator""_i16(unsigned long long value) noexcept -> i16 { return static_cast<i16>(value); }
		[[nodiscard]] constexpr auto operator""_i32(unsigned long long value) noexcept -> i32 { return static_cast<i32>(value); }
		[[nodiscard]] constexpr auto operator""_i64(unsigned long long value) noexcept -> i64 { return static_cast<i64>(value); }
		[[nodiscard]] constexpr auto operator""_sz(unsigned long long value) noexcept -> sz { return static_cast<sz>(value); }
		[[nodiscard]] constexpr auto operator""_ssz(unsigned long long value) noexcept -> ssz { return static_cast<ssz>(value); }
		[[nodiscard]] constexpr auto operator""_intptr(unsigned long long value) noexcept -> iptr { return static_cast<iptr>(value); }
		[[nodiscard]] constexpr auto operator""_uintptr(unsigned long long value) noexcept -> uptr { return static_cast<uptr>(value); }
		[[nodiscard]] constexpr auto operator""_ptrdiff(unsigned long long value) noexcept -> dptr { return static_cast<dptr>(value); }
	}
}