#pragma once
#include "Hz/Std/CompilerMacro.hpp"

namespace Hz
{	
#if defined(COMPILED_WITH_CLANG)
	#define FORCE_CONSTEXPR(expr) (__builtin_constant_p(expr) ? (expr) : (expr))
#elif defined(COMPILED_WITH_MSVC)
	#define FORCE_CONSTEXPR(expr) expr
#elif defined(COMPILED_WITH_GNU)
	#define FORCE_CONSTEXPR(expr) (__builtin_constant_p(expr) ? (expr) : (expr))
#endif
}