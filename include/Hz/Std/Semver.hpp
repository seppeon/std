/**
 * @file Semver.hpp
 * @brief Implement semantic versions as per semver.org's specification.
 */
#pragma once
#include <cstdint>
#include <limits>
#include <string>
#include <vector>
#include <string_view>

namespace Hz
{
	/**
	 * @brief A constant that when present in the `major`, `minor` or `patch`.
	 */
	inline constexpr auto invalid_semver_version = std::numeric_limits<std::uint32_t>::max();
	/**
	 * @brief A semver implementation that avoids bringing in another dependancy.
	 * 
	 * 
	 * @code
	 * <valid semver> ::= <version core>
	 *                  | <version core> "-" <pre-release>
	 *                  | <version core> "+" <build>
	 *                  | <version core> "-" <pre-release> "+" <build>
	 * @endcode
	 */
	struct Semver
	{
		std::uint32_t major = invalid_semver_version;
		std::uint32_t minor = invalid_semver_version;
		std::uint32_t patch = invalid_semver_version;
		std::string prerelease;
		std::string meta; 
		/**
		 * @brief Check if this is a valid semver.
		 * 
		 * @return true The semver can be used.
		 * @return false The semver isn't valid, and can't be used.
		 */
		[[nodiscard]] bool IsValid() const noexcept;
		/**
		 * @brief Get the semver string.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] auto ToString() const noexcept -> std::string;
	};
	/**
	 * @brief Compare two semvers.
	 * 
	 * @note Build metadata MUST be ignored when determining version precedence (https://semver.org/#spec-item-10).
	 * This is based on https://semver.org/#spec-item-11.
	 * 
	 * @param lhs Left hand operand.
	 * @param rhs Right hand operand.
	 * @return true lhs is less than rhs.
	 * @return false lhs is not less than rhs.
	 */
	[[nodiscard]] bool operator<(Semver const & lhs, Semver const & rhs);
	[[nodiscard]] bool operator>(Semver const & lhs, Semver const & rhs);
	[[nodiscard]] bool operator<=(Semver const & lhs, Semver const & rhs);
	[[nodiscard]] bool operator>=(Semver const & lhs, Semver const & rhs);
	[[nodiscard]] bool operator==(Semver const & lhs, Semver const & rhs);
	/**
	 * @brief The result of `SemverFromString`.
	 */
	struct ParseSemverResult
	{
		std::string_view remaining; ///< The remaining string.
		Semver semver;				///< The semver produced.
		/**
		 * @brief Check if this is a valid semver.
		 * 
		 * @return true The semver can be used.
		 * @return false The semver isn't valid, and can't be used.
		 */
		[[nodiscard]] bool IsValid() const noexcept;
	};
	/**
	 * @brief Parse the `input` and produce a semver structure.
	 * 
	 * @todo Rename this, its named poorly.
	 * 
	 * @param input The string to parse.
	 * @return The semver.
	 */
	[[nodiscard]] auto SemverFromString(std::string_view input) -> ParseSemverResult;
	/**
	 * @brief Get the Current Version From Macro object
	 * 
	 * @param input The string to parse.
	 * @return The semver.
	 */
	[[nodiscard]] auto GetSemverFromString(std::string_view input) -> Semver;
	/**
	 * @brief Get this applications current version.
	 * 
	 * @return The semver of this application.
	 */
	[[nodiscard]] auto GetCurrentVersion() -> Semver;
	/**
	 * @brief The result of finding a string.
	 */
	struct SemverStringResult
	{
		std::size_t index = 0;  ///< The remaining string.
		std::size_t length = 0; ///< The string length.
		Semver semver;          ///< The semver produced.
		/**
		 * @brief Check if this is a valid semver.
		 * 
		 * @return true The semver can be used.
		 * @return false The semver isn't valid, and can't be used.
		 */
		[[nodiscard]] bool IsValid() const noexcept;
		/**
		* @brief Compare two semver results.
		* 
		* @retval true When equal.
		* @retval false When not equal.
		*/
		[[nodiscard]] friend bool operator==(SemverStringResult const &, SemverStringResult const &) = default;
	};
	/**
	 * @brief A list of semver string results.
	 */
	using SemverStringResultList = std::vector<SemverStringResult>;
	/**
	 * @brief Find the first semver version string from the given `input`.
	 * 
	 * @param input The string to parse.
	 * @return The semver version string.
	 */
	[[nodiscard]] auto FindFirstSemverString(std::string_view input) -> SemverStringResult;
	/**
	 * @brief Find all the valid semvers.
	 * 
	 * @param input The string to parse.
	 * @return The list of semver strings.
	 */
	[[nodiscard]] auto FindAllSemverStrings(std::string_view input) -> SemverStringResultList;
}