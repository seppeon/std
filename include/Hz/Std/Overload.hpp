#pragma once
#include <Hz/Traits/FnTraits.hpp>
#include <type_traits>

namespace Hz
{
	template <typename T, typename Ret = GetFnResult<T>, typename Args = GetFnArgs<T>>
	struct OverloadWrap;
	template <typename T, typename R, typename ... Args>
	struct OverloadWrap<T, R, TL<Args...>>
	{
		T fn;
		constexpr decltype(auto) operator()( Args ... args ) const noexcept(noexcept(fn(args ...))) { return fn( args ... ); }
	};

	template <typename Fn>
	struct OverloadBaseImpl
	{
		using type = OverloadWrap<Fn>;
	};
	template <typename Fn> requires (std::is_class_v<Fn>)
	struct OverloadBaseImpl<Fn>
	{
		using type = Fn;
	};

	template <typename Fn>
	using OverloadBase = typename OverloadBaseImpl<Fn>::type;

	template <typename ... Fns>
	struct Overload : OverloadBase<std::remove_reference_t<Fns>>...
	{
		using OverloadBase<std::remove_reference_t<Fns>>::operator()...;
	};

	template <typename ... Fns>
	Overload(Fns...)->Overload<Fns...>;
}