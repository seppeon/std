#pragma once
#include "Hz/Std/Fold.hpp"
#include <cmath>
#include <type_traits>
#include <utility>
#include <limits>
#include <concepts>

namespace Hz
{
	template <typename Val>
	[[nodiscard]] constexpr bool Or(Val const & val) noexcept
	{
		return static_cast<bool>(val);
	}

	template <typename Val>
	[[nodiscard]] constexpr bool And(Val const & val) noexcept
	{
		return static_cast<bool>(val);
	}

	template <typename ... Vals>
	[[nodiscard]] constexpr bool Or(Vals const & ... vals) noexcept
	{
		return (vals || ...);
	}

	template <typename ... Vals>
	[[nodiscard]] constexpr bool And(Vals const & ... vals) noexcept
	{
		return (vals && ...);
	}

	template <typename T>
	[[nodiscard]] constexpr T IfTrueOne(bool input) noexcept
	{
		return (input) ? T{1} : T{0};
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr bool AnyOf(T const & input, Args const & ... tests) noexcept
	{
		return (... or (input == tests));
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr bool XOf(std::size_t x, T const & input, Args const & ... tests) noexcept
	{
		return (IfTrueOne<size_t>(input == std::forward<Args>(tests)) + ...) == x;
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr bool OneOf(T const & input, Args const & ... tests) noexcept
	{
		return XOf(1, input, tests...);
	}

	namespace Impl
	{
		template <typename F, typename ... Args>
		struct ImplImplFront{ using type = F; };

		template <typename ... Args>
		using ImplFront = typename ImplImplFront<Args...>::type;
	}

	template <typename T, typename ... Args>
	concept EqualityComparable = requires(T const & input, Args const & ... tests)
	{
		{ (... && (input == tests)) } -> std::same_as<bool>;
	};

	template <typename T, typename ... Args>
		requires EqualityComparable<T, Args...>
	[[nodiscard]] constexpr bool AllOf(T const & input, Args const & ... tests) noexcept 
	{
		return (... && (input == tests));
	}

	template <typename T, typename C>
	concept InequalityLoopable = requires(T const & input, C const & container)
	{
		{ container.begin() };
		{ container.end() };
		{ input != *container.begin() };
	};

	template <typename T, typename C>
		requires InequalityLoopable<T, C>
	[[nodiscard]] constexpr bool AllOf(T const & input, C const & container) noexcept
	{
		for (auto & v : container) if (input != v) return false;
		return true; 
	}

	template <typename C>
	concept BooleanLoopable = requires(C const & container)
	{
		{ container.begin() };
		{ container.end() };
		{ *container.begin() } -> std::convertible_to<bool>;
	};

	template <typename Container> 
		requires BooleanLoopable<Container>
	[[nodiscard]] constexpr bool AllOf(Container const & container) noexcept
	{
		for (auto & v : container) if (not v) return false;
		return true; 
	}

	template <typename T>
	[[nodiscard]] constexpr bool Between(T const & value, T const & minimum, T const & maximum) noexcept
	{
		return (minimum <= value) and (value <= maximum);
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr std::size_t CountEqual(T const & input, Args const && ... tests) noexcept
	{
		return (MakeTrueOne(input == std::forward<Args>(tests)) + ...);
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr std::size_t CountNotEqual(T const & input, Args const && ... tests) noexcept
	{
		return (MakeTrueOne(input != std::forward<Args>(tests)) + ...);
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr std::size_t CountGreater(T const & input, Args const & ... tests) noexcept
	{
		return (MakeTrueOne(input > std::forward<Args>(tests)) + ...);
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr std::size_t CountGreaterOrEqual(T const & input, Args const & ... tests) noexcept
	{
		return (MakeTrueOne(input >= std::forward<Args>(tests)) + ...);
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr std::size_t CountLess(T const & input, Args const & ... tests) noexcept
	{
		return (MakeTrueOne(input < std::forward<Args>(tests)) + ...);
	}
	template <typename T, typename ... Args>
	[[nodiscard]] constexpr std::size_t CountLessOrEqual(T const & input, Args const & ... tests) noexcept
	{
		return (MakeTrueOne(input <= std::forward<Args>(tests)) + ...);
	}
	template <typename ... Args> requires (sizeof...(Args) > 1)
	[[nodiscard]] constexpr auto Minimum(Args && ... args) noexcept -> std::common_reference_t<Args...>
	{
		return FoldL([](auto && lhs, auto && rhs)->decltype(auto){ return (not (rhs < lhs)) ? HZ_FWD(lhs) : HZ_FWD(rhs); }, HZ_FWD(args)...);
	}
	template <typename ... Args> requires (sizeof...(Args) > 1)
	[[nodiscard]] constexpr auto Maximum(Args && ... args) noexcept -> std::common_reference_t<Args...>
	{
		return FoldL([](auto && lhs, auto && rhs)->decltype(auto){ return (lhs > rhs) ? HZ_FWD(lhs) : HZ_FWD(rhs); }, HZ_FWD(args)...);
	}
	template <typename T>
	[[nodiscard]] constexpr auto Minimum(T && a) noexcept -> T
	{
		return HZ_FWD(a);
	}
	template <typename T>
	[[nodiscard]] constexpr auto Maximum(T && a) noexcept -> T
	{
		return HZ_FWD(a);
	}

	template <typename T>
	[[nodiscard]] constexpr auto Abs(T const & value) noexcept -> std::decay_t<T>
	{
		using type = std::decay_t<T>;
		if constexpr (std::is_signed_v<T>)
		{
			if (std::is_constant_evaluated())
			{
				return value >= 0 ? value : -value;
			}
			else
			{
				return std::abs(value);
			}
		}
		else
		{
			return value;
		}
	}

	template <typename T>
	[[nodiscard]] constexpr T ClampZeroOrMore(T const & count) noexcept
	{
		return Maximum(static_cast<T>(0), count);
	}
	template <typename T>
	[[nodiscard]] constexpr T ClampOneOrMore(T const & count) noexcept
	{
		return Maximum(static_cast<T>(1), count);
	}
	template <typename T>
	[[nodiscard]] constexpr T Clamp(T const & value, T const & min, T const & max) noexcept
	{
		if (value < min) return min;
		if (value > max) return max;
		return value;
	}
	template <typename T>
	[[nodiscard]] constexpr bool NearlyEquals(T const & lhs, T const & rhs, T const & epsilon = std::numeric_limits<T>::epsilon() ) noexcept
	{
		if (std::isfinite(lhs) && std::isfinite(rhs))
		{
			if (lhs == rhs) return true;
			auto epi = Abs(epsilon * Maximum(lhs, rhs));
			return Between(rhs, (lhs - epi), (lhs + epi));
		}
		return false;
	}

	template <typename T>
	[[nodiscard]] constexpr bool IsZero(T const & input) noexcept
	{
		return NearlyEquals<T>(input, 0);
	}
}