#pragma once
#include <Hz/Traits/Fwd.hpp>
#include "Hz/Std/AlwaysInline.hpp"
#include <Hz/TL/TLSplit.hpp>
#include <Hz/TL/TLCommon.hpp>
 
namespace Hz
{
	namespace Impl
	{
		template <typename HeadList, typename TailList>
		struct ArgsAtIndex;
		template <typename ... Head, typename ... Tail>
		struct ArgsAtIndex<TL<Head...>, TL<Tail...>>
		{
			[[nodiscard]] HZ_ALWAYS_INLINE static constexpr decltype(auto) Run(Head ..., auto && arg, Tail ...) noexcept
			{
				return HZ_FWD(arg);
			}
		};
		struct ArgEater { HZ_ALWAYS_INLINE constexpr ArgEater(auto &&) noexcept {} };
		template <typename T>
		using EatArg = ArgEater;
	}

	template <std::size_t I, typename ... Args>
	[[nodiscard]] HZ_ALWAYS_INLINE constexpr decltype(auto) ArgsAtIndex(Args && ... args) noexcept
	{
		using types = TL<Impl::EatArg<Args>...>;
		using split = TLSplit<types, I>;
		using lhs = typename split::lhs;
		using rhs = TLPopFront<typename split::rhs>;
		return Impl::ArgsAtIndex<lhs, rhs>::Run(HZ_FWD(args)...);
	}
}