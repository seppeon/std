#pragma once
#include "Hz/Std/Apply.hpp"
#include "Hz/Std/Variant.hpp"
#include "Hz/Std/Lifetime.hpp"
#include <Hz/Traits/Fwd.hpp>
#include <Hz/TL/TLCombine.hpp>
#include <Hz/TL/TTLCommon.hpp>
#include <Hz/Traits/CArrayLen.hpp>
#include <Hz/Traits/Concepts.hpp>
#include <Hz/Traits/ConceptsPointer.hpp>
#include <Hz/Traits/ValueType.hpp>
#include <Hz/Traits/TryTypes.hpp>
#include <cassert>
#include <type_traits>
#include <cstring>

namespace Hz
{
	namespace Impl
	{
		template <typename T>
		void MemcpySignature(T * dst, T const * src);
	}

	template <typename Src, typename Dst>
	struct SD{};

	template <typename Src, typename Dst>
	struct Copy;

	enum class CopyType
	{
		TriviallyCopy,
		AssignableCopy,
		SmartPointerAssignCopy,
		SmartPointerCopy,
		ReconstructCopy,
		TupleCopy,
		VariantEmplaceCopy,
		StdCopy,
		IteratorAssignCopy,
	};

	template <typename Src, typename Dst>
	concept TriviallyCopyableInto = (
		std::is_trivially_copyable_v<std::remove_reference_t<Src>>
		and std::is_trivially_copyable_v<std::remove_reference_t<Dst>>
		and requires(Src && src, Dst && dst)
		{
			{ Impl::MemcpySignature(std::addressof(dst), std::addressof(src)) };
		}
	);
	template <typename Src, typename Dst>
	concept AssignableCopyable = requires(Src const & src, Dst & dst)
	{
		{ dst = HZ_FWD(src) };
	};
	template <typename Src, typename Dst>
	concept ReconstructableCopyable = requires(Src const & input)
	{
		{ Dst{ HZ_FWD(input) } };
	};
	template <typename Src, typename Dst>
	concept TupleCopyable = (
		TupleLike<Src> and
		// Where `Src`, is:
		// - `Tup<Srcs...>`
		// Produces:
		// - std::is_constructible<std::remove_reference_t<Dst>, Srcs...>
		Hz::TLCombine
		<
			std::is_constructible,
			// Produces `std::is_constructible<std::remove_reference_t<Dst>>`
			std::is_constructible<std::remove_reference_t<Dst>>, 
			// This produces `std::is_constructible<Srcs...>`
			Hz::TLTranslate
			<
				std::remove_reference_t<Src>,
				std::is_constructible
			> 
		>::value
	);
	template <typename Src, typename Dst>
	concept SmartPointerAssignCopyable = (
		SmartPointer<Dst> and
		requires(Src const & src, Dst & dst)
		{
			{ dst = std::remove_reference_t<Dst>{ new std::remove_cvref_t<ValueType<std::remove_reference_t<Dst>>>(src) } };
		}
	);
	template <typename Src, typename Dst>
	concept SmartPointerCopyable = (
		PointerLike<Src> and
		SmartPointer<Dst> and
		requires(Src const & src, Dst & dst)
		{
			{ dst = std::remove_reference_t<Dst>{ new std::remove_cvref_t<ValueType<std::remove_reference_t<Src>>>(*src) } };
		}
	);
	template <typename Src, typename Dst>
	concept VariantEmplaceCopyable = (
		IsVariant<std::remove_reference_t<Dst>> and requires(Dst dst, Src const & src)
		{
			{ dst.Emplace(Type<Src>{}, src) };
		}
	);
	template <typename Src, typename Dst>
	concept StdCopyable = (
		std::input_iterator<std::ranges::iterator_t<Src>> and
		std::output_iterator<std::ranges::iterator_t<Dst>, ValueType<Src>> and
		(HasArrayLength<Src> and HasArrayLength<Dst>) and
		(get_array_length_v<Src> == get_array_length_v<Dst>) and
		(requires(Src const & src, Dst & dst)
		{
			{ std::copy(std::begin(src), std::end(src), std::begin(dst)) };
		})
	);

	template <typename T> 
	struct TriviallyCopy;
	template <typename Src, typename Dst>  requires TriviallyCopyableInto<Src, Dst>
	struct TriviallyCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::TriviallyCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				if (std::is_constant_evaluated())
				{
					ReconstructAt(&dst, src);
				}
				else
				{
					std::memcpy( std::addressof(dst), std::addressof(src), sizeof(Src) );
				}
			}
		};
	};
	template <typename T>
	struct AssignableCopy;
	template <typename Src, typename Dst>  requires AssignableCopyable<Src, Dst>
	struct AssignableCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::AssignableCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				dst = HZ_FWD(src);
			}
		};
	};
	template <typename T>
	struct ReconstructCopy;
	template <typename Src, typename Dst> requires (ReconstructableCopyable<Src, Dst>)
	struct ReconstructCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::ReconstructCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				ReconstructAt(std::addressof(dst), HZ_FWD(src));
			}
		};
	};
	template <typename T>
	struct TupleCopy;
	template <typename Src, typename Dst> requires TupleCopyable<Src, Dst>
	struct TupleCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::TupleCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				Apply([&](auto && ... args)
					{
						Copy(Dst{ HZ_FWD(args)... }, HZ_FWD(dst));
					},
					src
				);
			}
		};
	};
	template <typename T>
	struct SmartPointerAssignCopy;
	template <typename Src, typename Dst> requires SmartPointerAssignCopyable<Src, Dst>
	struct SmartPointerAssignCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::SmartPointerAssignCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				dst = Dst{ new std::remove_cvref_t<ValueType<std::remove_reference_t<Dst>>>{ src } };
			}
		};
	};
	template <typename T>
	struct SmartPointerCopy;
	template <typename Src, typename Dst> requires SmartPointerCopyable<Src, Dst>
	struct SmartPointerCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::SmartPointerCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				assert(src != nullptr);
				dst = Dst{ new std::remove_cvref_t<ValueType<std::remove_reference_t<Src>>>{ *src } };
			}
		};
	};
	template <typename T>
	struct VariantEmplaceCopy;
	template <typename Src, typename Dst> requires VariantEmplaceCopyable<Src, Dst>
	struct VariantEmplaceCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::VariantEmplaceCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				// At this point, assign has already failed.
				dst.Emplace(Type<Src>{}, HZ_FWD(src));
			}
		};
	};
	template <typename T>
	struct StdCopy;
	template <typename Src, typename Dst> requires StdCopyable<Src, Dst>
	struct StdCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::StdCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				std::copy(std::begin(src), std::end(src), std::begin(dst));
			}
		};
	};

	template <typename Src, typename Dst>
	concept IteratorAssignCopiable = requires(Src && src, Dst && dst)
	{
		{ HZ_FWD(dst) = std::remove_cvref_t<Dst>{ std::begin(src), std::end(src) } };
	};

	template <typename T>
	struct IteratorAssignCopy;
	template <typename Src, typename Dst> requires IteratorAssignCopiable<Src, Dst>
	struct IteratorAssignCopy<SD<Src, Dst>>
	{
		struct type
		{
			static constexpr auto copy_type = CopyType::IteratorAssignCopy;

			constexpr type() = default;
			constexpr type(Src const & src, Dst & dst)
			{
				dst = std::remove_cvref_t<Dst>{std::begin(src), std::end(src)};
			}
		};
	};

	template <typename Src, typename Dst>
	using CopyBase = TryTypes<
		SD<std::remove_cvref_t<Src>, std::remove_cvref_t<Dst>>,
		TriviallyCopy,
		AssignableCopy,
		TupleCopy,
		SmartPointerAssignCopy,
		SmartPointerCopy,
		VariantEmplaceCopy,
		StdCopy,
		IteratorAssignCopy,
		ReconstructCopy
	>;
	template <typename Src, typename Dst>
	struct Copy : CopyBase<Src, Dst>::type
	{
		using base = CopyBase<Src, Dst>;
		using base::base;
	};
	template <typename Src, typename Dst>
	Copy(Src&&, Dst&&)->Copy<Src, Dst>;
}