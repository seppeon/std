#pragma once
#include <Hz/Traits/Fwd.hpp>
#include "Hz/Std/Lifetime.hpp"
#include "Hz/Std/AlwaysInline.hpp"
#include <Hz/TL/TLCommon.hpp>
#include <Hz/TL/TLAtIndex.hpp>

namespace Hz
{
	struct UnionError{};

	template <std::size_t I, typename ... Args>
	union UnionElems;

	template <std::size_t I, typename Front>
	union UnionElems<I, Front>
	{
		using front_type = Front;
		using other_type = UnionError;
		static constexpr std::size_t index = I;

		front_type front;
	 	other_type others;

		constexpr UnionElems(){}
		constexpr ~UnionElems(){}
	};

	template <std::size_t I, typename Front, typename ... Args>
	union UnionElems<I, Front, Args...>
	{
		using front_type = Front;
		using other_type = UnionElems<I + 1, Args...>;
		static constexpr std::size_t index = I;

		front_type front;
		other_type others;

		constexpr UnionElems(){}
		constexpr ~UnionElems(){}
	};

	template <typename T>
	inline constexpr bool IsUnionElemsImpl = false;
	template <std::size_t Index, typename ... Args>
	inline constexpr bool IsUnionElemsImpl<UnionElems<Index, Args...>> = true;

	template <typename T, std::size_t I>
	using UnionElemType = TLAtIndex<typename std::remove_cvref_t<T>::list, I>;

	template <typename T>
	inline constexpr std::size_t UnionElemsIndex = std::remove_cvref_t<T>::index;

	template <typename T>
	concept IsUnionElems = IsUnionElemsImpl<std::remove_cvref_t<T>>;

	template <std::size_t I, IsUnionElems T>
	[[nodiscard]] HZ_ALWAYS_INLINE constexpr auto * GetUnionElemCtor(T * elems) noexcept
	{
		if constexpr (I == UnionElemsIndex<T>)
			return std::addressof(elems->front);
		else
			return GetUnionElemCtor<I>(ConstructAt(std::addressof(elems->others)));
	}

	template <std::size_t I, IsUnionElems T>
	HZ_ALWAYS_INLINE constexpr void GetUnionElemDtor(T * elems) noexcept
	{
		if constexpr (I == UnionElemsIndex<T>)
		{
			DestroyAt(std::addressof(elems->front));
		}
		else
		{
			auto * ptr = std::addressof(elems->others);
			GetUnionElemCtor<I>(ptr);
			DestroyAt(ptr);
		}
	}

	template <std::size_t I, IsUnionElems T>
	[[nodiscard]] HZ_ALWAYS_INLINE constexpr auto * GetUnionElemAccess(T * elems) noexcept
	{
		if constexpr (I == UnionElemsIndex<T>)
			return std::addressof(elems->front);
		else
			return GetUnionElemAccess<I>(std::addressof(elems->others));
	}
}