#pragma once
#include "Hz/Std/Invoke.hpp"
#include "Hz/Std/AlwaysInline.hpp"
#include <Hz/Traits/Fwd.hpp>
#include <utility>

namespace Hz
{
    namespace Impl
    {
        template <size_t I, typename Front, typename ... Tuples>
        HZ_ALWAYS_INLINE constexpr decltype(auto) GetArg( Front && front, Tuples && ... tuples)
        {
            using std::get;
            constexpr size_t front_size = std::tuple_size<std::remove_reference_t<Front>>::value;
            if constexpr (I < front_size)
            {
                return get<I>( HZ_FWD(front) );
            }
            else  
            {
                return GetArg<I - front_size>(HZ_FWD(tuples)...);
            }
        }

        template <typename F, size_t... I, typename ... Tuples>
        HZ_ALWAYS_INLINE constexpr decltype(auto) ApplyImpl(F&& f, std::index_sequence<I...>, Tuples && ... tuples)
        {
            return Invoke(HZ_FWD(f), GetArg<I>(tuples...)...);
        }
    }
 
    template <typename F, typename ... Tuples>
    HZ_ALWAYS_INLINE constexpr decltype(auto) Apply(F&& f, Tuples&& ... tuples)
    {
        constexpr size_t total_items = (std::tuple_size<std::remove_reference_t<Tuples>>::value + ...);
        return Impl::ApplyImpl(HZ_FWD(f), std::make_index_sequence<total_items>{}, HZ_FWD(tuples)... );
    }
}