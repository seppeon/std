#pragma once
#include "Hz/Std/AlwaysInline.hpp"
#include "Hz/Std/Invoke.hpp"
#include <Hz/Traits/RemoveRValueRef.hpp>
#include <Hz/Traits/Fwd.hpp>
#include <Hz/Traits/ValueType.hpp>
#include <Hz/Traits/ConceptsIterator.hpp>
#include <utility>
#include <type_traits>

namespace Hz
{
	template <typename L, typename R>
	struct FoldWrap;

	template <typename T>
	inline constexpr auto is_fold_wrap_v = false;
	template <typename L, typename R>
	inline constexpr auto is_fold_wrap_v<FoldWrap<L, R>> = true;

	template <typename T>
	concept IsFoldWrap = is_fold_wrap_v<T>;
	template <typename T>
	concept IsNFoldWrap = !is_fold_wrap_v<T>;

	namespace Impl
	{
		template <typename F, typename T>
		struct FoldResult;
	}
	template <typename F, typename T>
	using FoldResult = typename Impl::FoldResult<F, T>::type;

	template <typename F, typename L, typename R>
	concept RFoldContainer = ForIterable<L> && requires(F fn, FoldResult<F, L> && lhs, FoldResult<F, R> && rhs)
	{
		{ fn(*std::begin(HZ_FWD(lhs)), HZ_FWD(rhs)) };
	};

	template <typename F, typename L, typename R>
	concept LFoldContainer = ForIterable<R> && requires(F fn, FoldResult<F, L> && lhs, FoldResult<F, R> && rhs)
	{
		{ fn(HZ_FWD(lhs), *std::begin(HZ_FWD(rhs))) };
	};

	template <typename F, typename L, typename R>
	concept NotFoldContainer = not LFoldContainer<F, L, R> and not RFoldContainer<F, L, R>;

	namespace Impl
	{
		template <typename F, typename L, typename R>
		struct FoldInvoke {
			using type = std::invoke_result_t<F, L, R>;
		};
		template <typename F, typename L, typename R> requires RFoldContainer<F, L, R>
		struct FoldInvoke<F, L, R> {
			using type = R;
		};
		template <typename F, typename L, typename R> requires LFoldContainer<F, L, R>
		struct FoldInvoke<F, L, R> {
			using type = L;
		};

		template <typename F, typename T>
		struct FoldResult {
			using type = T;
		};
		template <typename F, IsNFoldWrap L, IsNFoldWrap R>
		struct FoldResult<F, FoldWrap<L, R>> {
			using type = typename FoldInvoke<F, L, R>::type;
		};
		template <typename F, IsFoldWrap L, IsNFoldWrap R>
		struct FoldResult<F, FoldWrap<L, R>> {
			using lhs = typename FoldResult<F, L>::type;
			using type = typename FoldResult<F, FoldWrap<lhs, R>>::type;
		};
		template <typename F, IsNFoldWrap L, IsFoldWrap R>
		struct FoldResult<F, FoldWrap<L, R>> {
			using rhs = typename FoldResult<F, R>::type;
			using type = typename FoldResult<F, FoldWrap<L, rhs>>::type;
		};
		template <typename F, IsFoldWrap L, IsFoldWrap R>
		struct FoldResult<F, FoldWrap<L, R>> {
			using lhs = typename FoldResult<F, L>::type;
			using rhs = typename FoldResult<F, R>::type;
			using type = typename FoldResult<F, FoldWrap<lhs, rhs>>::type;
		};
	}

	template <typename L, typename R>
	struct FoldWrap
	{
		L lhs;
		R rhs;

		template <typename F>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr auto Left(F fn) const -> FoldResult<F, L>
		{
			if constexpr (is_fold_wrap_v<L>)
			{
				return lhs.Apply(fn);
			}
			else
			{
				return lhs;
			}
		}
		template <typename F>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr auto Right(F fn) const -> FoldResult<F, R>
		{
			if constexpr (is_fold_wrap_v<R>)
			{
				return rhs.Apply(fn);
			}
			else
			{
				return rhs;
			}
		}

		template <typename F>
		[[nodiscard]] HZ_ALWAYS_INLINE constexpr auto Apply(F fn) const -> FoldResult<F, FoldWrap<L, R>>
		{
			using lhs_type = FoldResult<F, L>;
			using rhs_type = FoldResult<F, R>;
			if constexpr (std::is_invocable_v<F, lhs_type, rhs_type>)
			{
				return Invoke(fn, Left(fn), Right(fn));
			}
			else if constexpr (LFoldContainer<F, lhs_type, rhs_type>)
			{
				auto const & cont = Right(fn);
				auto beg = std::begin(cont);
				auto end = std::end(cont);
				auto init = Left(fn);
				for ( ; beg != end; ++beg)
				{
					init = fn(std::move(init), *beg);
				}
				return init;
			}
			else if constexpr (RFoldContainer<F, lhs_type, rhs_type>)
			{
				auto const & cont = Left(fn);
				auto beg = std::rbegin(cont);
				auto end = std::rend(cont);
				auto init = Right(fn);
				for ( ; beg != end; ++beg)
				{
					init = fn(*beg, std::move(init));
				}
				return init;
			}
		}
	};
	template <typename L, typename R>
	FoldWrap(L&&, R&&)->FoldWrap<Hz::RemoveRValueRef<L>, Hz::RemoveRValueRef<R>>;

	template <typename A, typename B>
	[[nodiscard]] constexpr auto FoldLWrap(A && a, B && b)
	{
		return FoldWrap{ HZ_FWD(a), HZ_FWD(b) };
	}
	template <typename A, typename B, typename... Args> requires (sizeof...(Args) > 0)
	[[nodiscard]] constexpr auto FoldLWrap(A && a, B && b, Args && ... args )
	{
		return FoldLWrap(FoldWrap{ HZ_FWD(a), HZ_FWD(b) }, HZ_FWD(args)...);
	}
	template <typename A, typename B>
	[[nodiscard]] constexpr auto FoldRWrap(A && a, B && b)
	{
		return FoldWrap{HZ_FWD(a), HZ_FWD(b)};
	}
	template <typename A, typename... Args> requires (sizeof...(Args) > 0)
	[[nodiscard]] constexpr auto FoldRWrap(A && a, Args &&... args )
	{
		return FoldWrap{HZ_FWD(a), FoldRWrap(HZ_FWD(args)...)};
	}

	template <typename F, typename ... Args>
	[[nodiscard]] constexpr auto FoldL(F fn, Args && ... args)
	{
		return FoldLWrap(HZ_FWD(args)...).Apply(fn);
	}

	template <typename F, typename ... Args>
	[[nodiscard]] constexpr auto FoldR(F fn, Args && ... args)
	{
		return FoldRWrap(HZ_FWD(args)...).Apply(fn);
	}
}