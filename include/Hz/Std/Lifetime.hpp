#pragma once
#include <Hz/Traits/Fwd.hpp>
#include "Hz/Std/AlwaysInline.hpp"
#include "Hz/Std/Apply.hpp"
#include "Hz/Std/AgTup.hpp"
#include <memory>

namespace Hz
{
	template <typename T, typename ... Args>
	HZ_ALWAYS_INLINE constexpr T * ConstructAt(T * ptr, Args && ... args) noexcept
	{
		return std::construct_at(ptr, HZ_FWD(args)...);
	}

	template <typename T>
	HZ_ALWAYS_INLINE constexpr T * DestroyAt(T * ptr) noexcept
	{
		return (std::destroy_at(ptr), ptr);
	}

	template <typename T, typename ... Args>
	HZ_ALWAYS_INLINE constexpr T * ReconstructAt(T * ptr, Args && ... args) noexcept
	{
		return ConstructAt(DestroyAt(ptr), HZ_FWD(args)...);
	}

	template <typename F, typename ... Args>
	struct CallbackOnConvert
	{
		using result_type = std::invoke_result_t<F, Args...>;

		F callback;
		AgTup<Args...> args;

		[[nodiscard]] constexpr operator result_type() noexcept
		{
			return Apply(callback, args);
		}
	};

	template <typename T, typename F, typename ... Args>
	[[nodiscard]] HZ_ALWAYS_INLINE constexpr T * ConstructAtInplace(T * ptr, F && fn, Args && ... args) noexcept
	{
		return std::construct_at(ptr, CallbackOnConvert<F, Args...>{{ HZ_FWD(fn) }, { HZ_FWD(args)... }});
	}
}