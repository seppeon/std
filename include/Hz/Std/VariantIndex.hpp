#pragma once
#include <concepts>

namespace Hz
{
	template <typename T>
	concept GetVariantStdIndex = requires(T obj)
	{
		{ obj.index() } -> std::integral;
	};

	template <typename T>
	concept GetVariantHzIndex = requires(T obj)
	{
		{ obj.GetIndex() } -> std::integral;
	};

	template <typename C>
		requires (GetVariantStdIndex<C> or GetVariantHzIndex<C>)
	[[nodiscard]] constexpr std::size_t VariantIndex(C && c) noexcept
	{
		if constexpr (GetVariantStdIndex<C>)
		{
			return c.index();
		}
		else if constexpr (GetVariantHzIndex<C>)
		{
			return c.GetIndex();
		}
		else
		{
			return 1;
		}
	}
}