#pragma once
#include <Hz/Traits/Fwd.hpp>
#include <Hz/Traits/ConceptsStd.hpp>
#include <Hz/Traits/ConceptsStdLib.hpp>
#include <Hz/Traits/ConceptsCntLib.hpp>

namespace Hz
{
	template <typename C>
		requires (not Ptr<C>)
	constexpr decltype(auto) Data(C && c) noexcept
	{
		if constexpr (Cnt::HasData<C>)
		{
			return HZ_FWD(c).Data();
		}
		else if constexpr (Std::HasData<C>)
		{
			using std::data;
			return data(HZ_FWD(c));
		}
		else
		{
			return std::addressof(c);
		}
	}

    template <typename T>
    concept Datable = requires(T && obj)
    {
        { ::Hz::Data(obj) } -> Ptr;
    };

    template <typename T, typename Obj>
    concept DatableTo = requires(T && obj)
    {
        { ::Hz::Data(obj) } -> PtrTo<Obj>;
    };

	template<typename T>
	concept SupportsData = ( Std::HasData<T> or Cnt::HasData<T> );
}