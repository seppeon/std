#pragma once
#include <cstddef>
#include <utility>
#include <type_traits>

namespace Hz
{
	template <typename ... Args>
	struct NTup;
}

namespace std
{
	template <std::size_t I, class T>
	struct tuple_element;
	template <typename T>
	struct tuple_size;

	template<std::size_t I, class Head, class... Tail>
	struct tuple_element<I, ::Hz::NTup<Head, Tail...>>
		: tuple_element<I - 1, ::Hz::NTup<Tail...>>
	{};
	template<class Head, class... Tail>
	struct tuple_element<0, ::Hz::NTup<Head, Tail...>>
	{
		using type = Head;
	};
	template< class... Types >
	struct tuple_size<::Hz::NTup<Types...> > : std::integral_constant<std::size_t, sizeof...(Types)> {};
}

namespace std
{
    template < typename... Ts, typename... Us, template<typename> class Qual1, template<typename> class Qual2 >
		requires (
			(sizeof...(Ts) == sizeof...(Us)) and
			(std::common_reference_with<Ts, Us> and ...)
		)
    struct basic_common_reference<::Hz::NTup<Ts...>, ::Hz::NTup<Us...>, Qual1, Qual2>
    {
		using type = ::Hz::NTup<common_reference_t<Qual1<Ts>, Qual2<Us>>...>;
	};
}

namespace Hz
{
	template <typename, typename>
	struct NBase;

	template <typename T, std::size_t I>
	struct NTupBase
	{
		T val = nullptr;

		[[nodiscard]] friend constexpr auto RefAt(NTupBase<T, I> & tup_base, std::index_sequence<I>) noexcept -> T &
		{
			return tup_base.val;
		}
		[[nodiscard]] friend constexpr auto RefAt(NTupBase<T, I> const & tup_base, std::index_sequence<I>) noexcept -> T const &
		{
			return tup_base.val;
		}
	};

	template <template <typename...> class C, typename ... Args, std::size_t ... Is>
	struct NBase<C<Args...>, std::index_sequence<Is...>>
		: NTupBase<Args, Is>
		  ...
	{
	};

	template <typename F, typename ... Args>
	[[nodiscard]] constexpr decltype(auto) apply(F fn, NTup<Args...> const & args);

	// Please note, NTup is not intended for use by any library consumer, it has strange semantics
	// that made implementing NSpan easier.
	template <typename ... Args>
	struct NTup	: NBase<NTup<Args...>, std::make_index_sequence<sizeof...(Args)>>
	{
		using pointer = NTup<Args...>;
		using difference_type = std::make_signed_t<std::size_t>;
		using reference = NTup<std::add_lvalue_reference_t<std::remove_reference_t<Args>>...>;
		using const_reference = NTup<std::add_lvalue_reference_t<std::add_const_t<std::remove_reference_t<Args>>>...>;

		template <std::size_t I>
		[[nodiscard]] friend constexpr auto get(NTup & base) noexcept -> std::tuple_element_t<I, NTup> &
		{
			return RefAt(base, std::index_sequence<I>{});
		}

		template <std::size_t I>
		[[nodiscard]] friend constexpr auto get(NTup const & base) noexcept -> std::tuple_element_t<I, NTup> const &
		{
			return RefAt(base, std::index_sequence<I>{});
		}

		constexpr operator reference() const noexcept
		{
			return apply([](Args & ... args){ return reference{ args... }; }, *this);
		}

		constexpr operator const_reference() const noexcept requires(not std::same_as<reference, const_reference>)
		{
			return apply([](Args & ... args){ return const_reference{ args... }; }, *this);
		}

		[[nodiscard]] constexpr auto operator&() const noexcept -> NTup<std::add_pointer_t<Args>...>
		{
			return [&]<std::size_t ... Is>(std::index_sequence<Is...>) constexpr -> NTup<std::add_pointer_t<Args>...>
			{
				return NTup<std::add_pointer_t<Args>...>{ &get<Is>(*this) ... };
			}(std::make_index_sequence<sizeof...(Args)>());
		}

		[[nodiscard]] constexpr auto operator*() const noexcept -> NTup<std::remove_pointer_t<Args> &...> requires(std::is_pointer_v<Args> and ...)
		{
			return [&]<std::size_t ... Is>(std::index_sequence<Is...>) constexpr -> NTup<std::remove_pointer_t<Args> &...>
			{
				return NTup<std::remove_pointer_t<Args> &...>{ *get<Is>(*this) ... };
			}(std::make_index_sequence<sizeof...(Args)>());
		}

		[[nodiscard]] constexpr auto operator[](difference_type index) const noexcept -> NTup<std::remove_pointer_t<Args> &...> requires(std::is_pointer_v<Args> and ...)
		{
			return [&]<std::size_t ... Is>(std::index_sequence<Is...>) constexpr -> NTup<std::remove_pointer_t<Args> &...>
			{
				return NTup<std::remove_pointer_t<Args> &...>{ *(get<Is>(*this) + index) ... };
			}(std::make_index_sequence<sizeof...(Args)>());
		}

	private:
		template <std::size_t I>
		static constexpr bool less_recursive(NTup const & lhs, NTup const & rhs) noexcept
		{
			auto & l = get<I>(lhs);
			auto & r = get<I>(rhs);
			if constexpr (I == (sizeof...(Args) - 1))
			{
				return l < r;
			}
			else
			{
				if (l < r) return true;
				if (r < l) return false;
				return less_recursive<I+1>(lhs, rhs);
			}
		}
	public:
		[[nodiscard]] friend constexpr bool operator==(NTup const & lhs, NTup const & rhs) noexcept
		{
			return apply(
				[&](auto const &...ls) -> bool
				{
					return apply(
						[&](auto const &...rs) -> bool
						{
							return ((ls == rs) and ...);
						},
						rhs
					);
				},
				lhs
			);
		}
		[[nodiscard]] friend constexpr bool operator<(NTup const & lhs, NTup const & rhs) noexcept
		{
			return less_recursive<0>(lhs, rhs);
		}
	};
	template <typename ... Args>
	NTup(Args...)->NTup<Args...>;

	template <typename F, typename ... Args>
	[[nodiscard]] constexpr decltype(auto) apply(F fn, NTup<Args...> const & args)
	{
		return [&fn, &args]<std::size_t ... Is>(std::index_sequence<Is...>) constexpr -> decltype(auto)
		{
			return fn(get<Is>(args)...);
		}(std::make_index_sequence<sizeof...(Args)>());
	}
}