#pragma once
#include <Hz/Traits/Fwd.hpp>
#include "Hz/Std/AlwaysInline.hpp"
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template<class>
		constexpr bool is_reference_wrapper_v = false;
		template<class U>
		constexpr bool is_reference_wrapper_v<std::reference_wrapper<U>> = true;

		template<class C, class Pointed, class T1, class... Args>
		HZ_ALWAYS_INLINE constexpr decltype(auto) invoke_memptr( Pointed C::*f, T1 && t1, Args &&... args )
		{
			if constexpr ( std::is_function_v<Pointed> )
			{
				if constexpr ( std::is_base_of_v<C, std::decay_t<T1>> ) return ( HZ_FWD( t1 ).*f )( HZ_FWD( args )... );
				else if constexpr ( is_reference_wrapper_v<std::decay_t<T1>> )
					return ( t1.get().*f )( HZ_FWD( args )... );
				else
					return ( ( *HZ_FWD( t1 ) ).*f )( HZ_FWD( args )... );
			}
			else
			{
				static_assert( std::is_object_v<Pointed> && sizeof...( args ) == 0 );
				if constexpr ( std::is_base_of_v<C, std::decay_t<T1>> ) return HZ_FWD( t1 ).*f;
				else if constexpr ( is_reference_wrapper_v<std::decay_t<T1>> )
					return t1.get().*f;
				else
					return ( *HZ_FWD( t1 ) ).*f;
			}
		}
	}

	template<class F, class... Args>
	HZ_ALWAYS_INLINE constexpr auto Invoke(F&& f, Args&&... args) noexcept(std::is_nothrow_invocable_v<F, Args...>) -> std::invoke_result_t<F, Args...>
	{
		if constexpr (std::is_member_pointer_v<std::decay_t<F>>)
			return Impl::invoke_memptr(f, HZ_FWD(args)...);
		else
			return HZ_FWD(f)(HZ_FWD(args)...);
	}
}