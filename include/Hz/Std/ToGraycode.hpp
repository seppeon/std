#pragma once
#include <Hz/Traits/ConceptsStd.hpp>

namespace Hz
{
	template<UnsignedInt T>
	[[nodiscard]] constexpr T ToGraycode(T const & value) noexcept
	{
		return value ^ (value >> 1);
	}
}