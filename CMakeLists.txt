cmake_minimum_required(VERSION 3.22)
project(Hz_Std VERSION 0.4.1 LANGUAGES CXX DESCRIPTION "The standard library used by most other hz libraries.")

option(HZ_STD_TESTS_ENABLED  "Are tests ON or OFF?" OFF)
option(HZ_STD_CODE_COVERAGE  "Should tests be built with coverage?" OFF)
option(HZ_STD_BUILD_DOC      "Build documentation"  OFF)

if (HZ_STD_TESTS_ENABLED)
	add_subdirectory(test)
endif()
if (HZ_STD_BUILD_DOC)
	add_subdirectory(docs/doxygen)
endif()
find_package(Hz_TL REQUIRED)
find_package(Hz_Traits REQUIRED)

file(GLOB SRC_FILES CONFIGURE_DEPENDS src/*.cpp)
add_library(Hz_Std)
target_sources(Hz_Std PRIVATE ${SRC_FILES})
target_compile_features(Hz_Std PRIVATE cxx_std_23)
target_link_libraries(Hz_Std PUBLIC Hz::Traits Hz::TL)
target_compile_definitions(Hz_Std PUBLIC HZ_STD_LIB_VERSION="${PROJECT_VERSION}")
target_compile_definitions(Hz_Std PUBLIC HZ_STD_LIB_MAJOR_VERSION=${PROJECT_VERSION_MAJOR})
target_compile_definitions(Hz_Std PUBLIC HZ_STD_LIB_MINOR_VERSION=${PROJECT_VERSION_MINOR})
target_compile_definitions(Hz_Std PUBLIC HZ_STD_LIB_PATCH_VERSION=${PROJECT_VERSION_PATCH})
target_include_directories(Hz_Std
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)
add_library(Hz::Std ALIAS Hz_Std)

include(GNUInstallDirs)
include(cmake/install.cmake)
include(cmake/cpack.cmake)