cmake_minimum_required(VERSION 3.15)
project(PackageTest CXX)

find_package(Hz_Std CONFIG REQUIRED)

add_executable(PackageTest src/test.cpp)
target_compile_features(PackageTest PRIVATE cxx_std_23)
target_link_libraries(PackageTest PRIVATE Hz::Std)