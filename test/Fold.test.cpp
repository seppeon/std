#include "Hz/Std/Fold.hpp"
#include <cctype>
#include <sstream>
#include <tuple>
#include <vector>
#include <string>
#include <array>

template <typename ... Args>
std::ostream & operator<<(std::ostream & s, std::tuple<Args...> const & tup)
{
	std::apply([&](Args const & ... args)
	{
		((s << " " << args), ...);
	}, tup);
	return s;
}

#include <catch2/catch_all.hpp>

TEST_CASE("Can left fold packs", "[Hz][Functional]")
{
	CHECK(Hz::FoldL(std::plus<int>{}, 1, 2, 3) == 6);
	CHECK(Hz::FoldL(std::plus<int>{}, 1, std::array{2, 3, 4}, 5) == 15);

	auto const has_lower = [](bool result, char s){ return result or std::islower(s); };
	CHECK(Hz::FoldL(has_lower, false, '\0', std::string("HELLO"), "WORLD") == false);
	CHECK(Hz::FoldL(has_lower, false, '\0', std::string("HELLo"), "WORLD") == true);

	std::vector<std::tuple<int, int>> lpairs{};
	auto const add_plusl = [&](int lhs, int rhs)
	{
		lpairs.push_back({lhs, rhs});
		return lhs + rhs;
	};
	CHECK(Hz::FoldL(add_plusl, 1, std::array<int, 3>{2, 3, 4}, 5) == 15);
	CHECK(lpairs == std::vector<std::tuple<int, int>>
	{
		std::tuple<int, int>{1, 2},
		std::tuple<int, int>{3, 3},
		std::tuple<int, int>{6, 4},
		std::tuple<int, int>{10, 5}
	});

	std::vector<std::tuple<int, int>> rpairs{};
	auto const add_plusr = [&](int lhs, int rhs)
	{
		rpairs.push_back({lhs, rhs});
		return lhs + rhs;
	};
	CHECK(Hz::FoldR(add_plusr, 1, std::array<int, 3>{2, 3, 4}, 5) == 15);
	CHECK(rpairs == std::vector<std::tuple<int, int>>
	{
		std::tuple<int, int>{4, 5},
		std::tuple<int, int>{3, 9},
		std::tuple<int, int>{2, 12},
		std::tuple<int, int>{1, 14}
	});
}
