#include "Hz/Std/Overload.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Overload can have multiple functions of various types", "[Std]")
{
	static int last = 0;
	SECTION("Lambdas")
	{
		last = 0;
		Hz::Overload overload
		{
			[](int value)
			{
				last = 1;
			},
			[](bool value)
			{
				last = 2;
			},
			[](char value)
			{
				last = 3;
			}
		};
		overload(10);
		REQUIRE(last == 1);
		overload(true);
		REQUIRE(last == 2);
		overload('a');
		REQUIRE(last == 3);
	}
	SECTION("Free")
	{
		last = 0;
		Hz::Overload overload
		{
			+[](int value)
			{
				last = 1;
			},
			+[](bool value)
			{
				last = 2;
			},
			+[](char value)
			{
				last = 3;
			}
		};
		REQUIRE(last == 0);
		overload(10);
		REQUIRE(last == 1);
		overload(true);
		REQUIRE(last == 2);
		overload('a');
		REQUIRE(last == 3);
	}
}