#include "Hz/Std/Apply.hpp"
#include "Hz/Std/AgTup.hpp"
#include <catch2/catch_all.hpp>

using namespace Hz;

TEST_CASE("Apply", "[Fn::Apply]")
{
	constexpr AgTup test{1, 2, 3};
	auto sum = Apply([](auto ... args) noexcept { return (args + ...); }, test);
	REQUIRE(sum == 6);
}