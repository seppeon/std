#pragma once

struct Aggregate
{
	int a, b, c;
	friend constexpr bool operator==(Aggregate, Aggregate) = default;
};

struct NonAggregate
{
	int a, b, c;

	constexpr NonAggregate(int a, int b, int c) : a(a), b(b), c(c) {}

	friend constexpr bool operator==(NonAggregate, NonAggregate) = default;
};

struct OnlyConstructible
{
	int value = 0;
	OnlyConstructible(OnlyConstructible const & v) : value(v.value)
	{
	}
	explicit OnlyConstructible(int v) : value(v) {}

	OnlyConstructible(OnlyConstructible &&) = delete;
	OnlyConstructible& operator=(OnlyConstructible &&) = delete;
	OnlyConstructible& operator=(OnlyConstructible const &) = delete;
};