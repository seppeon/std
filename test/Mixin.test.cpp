#include "Hz/Std/Mixin.hpp"
#include <string>

#include <catch2/catch_all.hpp>

namespace
{
	using Hz::Mixin;
	using Hz::Mixer;
	using Hz::Ctor;
	using Hz::TType;

	template <typename T>
	struct AB : T
	{
		bool a, b;
	};

	template <typename T>
	struct CD : T
	{
		std::string c, d;
	};

	struct ABCD : Mixin<ABCD, AB, CD>
	{
		using base = Mixin;
		using base::base;
	};
}

TEST_CASE("Mixin can construct subelements", "[Hz::Mixin, Hz::Mixer]")
{
	ABCD abcd
	{
		Ctor{ TType<AB>{}, true, false },
		Ctor{ TType<CD>{}, "hello", "world" }
	};
}