#include "Hz/Std/SwitchCase.hpp"
#include <variant>
#include <catch2/catch_all.hpp>

using Hz::Switch, Hz::Case, Hz::Default, Hz::key, Hz::type_key;

TEST_CASE("Switch case callback", "[Hz::Switch, Hz::Case, Hz::Default, Hz::key]")
{
	constexpr auto opt_a =
	(
		Case{ key<4>, []{ return 104; } } |
		Case{ key<5>, []{ return 105; } } |
		Case{ key<6>, []{ return 106; } }
	);
	constexpr auto options =
	(
		opt_a |
		Case{ key<0>, []{ return 100; } } |
		Case{ key<1>, []{ return 101; } } |
		Case{ key<2>, []{ return 102; } } |
		Default{ []{ return 103; } }
	);

	REQUIRE(options(0) == 100);
	REQUIRE(options(1) == 101);
	REQUIRE(options(2) == 102);
	REQUIRE(options(3) == 103);
	REQUIRE(options(4) == 104);
}

TEST_CASE("Switch case", "[Hz::Switch, Hz::Case, Hz::Default, Hz::key]")
{
	constexpr auto opt_a =
	(
		Case{ key<4>, 104 } |
		Case{ key<5>, 105 } |
		Case{ key<6>, 106 }
	);
	constexpr auto options =
	(
		opt_a |
		Case{ key<0>, 100 } |
		Case{ key<1>, 101 } |
		Case{ key<2>, 102 } |
		Default{ 103 }
	);

	REQUIRE(options(0) == 100);
	REQUIRE(options(1) == 101);
	REQUIRE(options(2) == 102);
	REQUIRE(options(3) == 103);
	REQUIRE(options(4) == 104);
}