#include "Hz/Std/Copy.hpp"
#include "Hz/Std/Variant.hpp"
#include "TestTypes.hpp"
#include <list>
#include <memory>
#include <string>
#include <tuple>
#include <catch2/catch_all.hpp>
#include <variant>

TEST_CASE("Hz::Copy can trivially copy aggregate", "[Hz::Std]")
{
	Aggregate foo{};
	auto copier = Hz::Copy(Aggregate{1, 2, 3}, foo);
	using type = decltype(copier);
	CHECK(type::copy_type == Hz::CopyType::TriviallyCopy);
	CHECK(foo == Aggregate{1, 2, 3});
}

TEST_CASE("Hz::Copy can copy from a tuple to aggregate", "[Hz::Std]")
{
	Aggregate foo{};
	auto copier = Hz::Copy(std::tuple{1, 2, 3}, foo);
	using type = decltype(copier);
	CHECK(type::copy_type == Hz::CopyType::TupleCopy);
	CHECK(foo == Aggregate{1, 2, 3});
}

TEST_CASE("Hz::Copy can copy from a tuple to non-aggregate", "[Hz::Std]")
{
	NonAggregate foo{0, 0, 0};
	auto copier = Hz::Copy(std::tuple{1, 2, 3}, foo);
	using type = decltype(copier);
	CHECK(type::copy_type == Hz::CopyType::TupleCopy);
	CHECK(foo == NonAggregate{1, 2, 3});
}

TEST_CASE("Hz::Copy can copy from one iteratable container to another different type that supports copy assignment construction", "[Hz::Std]")
{
	std::vector<int> dst{};
	std::list<int> src{1, 2, 3};
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	CHECK(type::copy_type == Hz::CopyType::IteratorAssignCopy);
	CHECK(dst == std::vector<int>{1, 2, 3});
}

TEST_CASE("Hz::Copy can copy from one smart pointer to another", "[Hz::Std]")
{
	auto src = std::make_shared<int>(10);
	auto dst = std::make_unique<int>(123);
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	STATIC_CHECK(type::copy_type == Hz::CopyType::SmartPointerCopy);
	CHECK(dst.get() != src.get());
	CHECK(*dst == 10);
}

TEST_CASE("Hz::Copy can copy from a value to a smart pointer", "[Hz::Std]")
{
	auto src = int(10);
	auto dst = std::make_unique<int>(123);
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	STATIC_CHECK(type::copy_type == Hz::CopyType::SmartPointerAssignCopy);
	CHECK(dst.get() != &src);
	CHECK(*dst == 10);
}

TEST_CASE("Hz::Copy can copy between arrays", "[Hz::Std]")
{
	std::string src[3]{ "hello", "this", "is" };
	std::string dst[3]{};
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	STATIC_CHECK(type::copy_type == Hz::CopyType::StdCopy);
	CHECK(std::ranges::equal(src, dst));
}

TEST_CASE("Hz::Copy can copy into variants", "[Hz::Std]")
{
	int src = 10;
	std::variant<int, std::string> dst{};
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	STATIC_CHECK(type::copy_type == Hz::CopyType::AssignableCopy);
	REQUIRE(std::holds_alternative<int>(dst));
	CHECK(src == *std::get_if<int>(&dst));
}

TEST_CASE("Hz::Copy can copy into Variants", "[Hz::Std]")
{
	int src = 10;
	Hz::Variant<int, std::string> dst{[]{
		return std::string("wrong_type");
	}};
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	STATIC_CHECK(type::copy_type == Hz::CopyType::VariantEmplaceCopy);
	REQUIRE(HasType<int>(dst));
	CHECK(src == UnsafeGet<int>(dst));
}

TEST_CASE("Hz::Copy can construct non-copy assignable types through reconstruction", "[Hz::Std]")
{
	OnlyConstructible src(10);
	OnlyConstructible dst(9999);
	auto copier = Hz::Copy(src, dst);
	using type = decltype(copier);
	STATIC_CHECK(type::copy_type == Hz::CopyType::ReconstructCopy);
	CHECK(dst.value == 10);
}