#include "Hz/Std/AgTup.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("AgTup can compare with less")
{
	using Hz::AgTup;
	
	STATIC_REQUIRE(AgTup{0,1}<AgTup{1,2});
	STATIC_REQUIRE(AgTup{1,2}<AgTup{1,3});
	STATIC_REQUIRE(AgTup{1,3}<AgTup{2,4});
	STATIC_REQUIRE(AgTup{2,4}<AgTup{2,7});
	STATIC_REQUIRE(AgTup{2,7}<AgTup{3,6});
	STATIC_REQUIRE(AgTup{3,6}<AgTup{4,7});
	STATIC_REQUIRE(AgTup{4,7}<AgTup{5,7});
	STATIC_REQUIRE(AgTup{5,7}<AgTup{6,7});
	STATIC_REQUIRE_FALSE(AgTup{0,1}>AgTup{1,2});
	STATIC_REQUIRE_FALSE(AgTup{1,2}>AgTup{1,3});
	STATIC_REQUIRE_FALSE(AgTup{1,3}>AgTup{2,4});
	STATIC_REQUIRE_FALSE(AgTup{2,4}>AgTup{2,7});
	STATIC_REQUIRE_FALSE(AgTup{2,7}>AgTup{3,6});
	STATIC_REQUIRE_FALSE(AgTup{3,6}>AgTup{4,7});
	STATIC_REQUIRE_FALSE(AgTup{4,7}>AgTup{5,7});
	STATIC_REQUIRE_FALSE(AgTup{5,7}>AgTup{6,7});
	STATIC_REQUIRE(not (AgTup{2,7}>AgTup{3,6}));
}