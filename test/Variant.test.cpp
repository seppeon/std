#include "Hz/Std/Variant.hpp"
#include <catch2/catch_all.hpp>
#include <string>
#include <variant>

namespace
{
	template <typename ... Args>
	std::ostream & operator << (std::ostream & stream, Hz::Variant<Args...> const & variant)
	{
		Visit([&](auto const & value){ stream << value; }, variant);
		return stream;
	}
}

TEST_CASE("Variant can be identified by IsVariant")
{
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int>>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int, char>>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int> &>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int, char> &>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int> &&>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int, char> &&>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int> const &>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int, char> const &>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int> const &&>);
	STATIC_REQUIRE(Hz::IsVariant<Hz::Variant<int, char> const &&>);
}

TEST_CASE("Variant can compare elements")
{
	using Hz::Variant, Hz::Elem;
	SECTION("valid")
	{
		SECTION("equal")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{10} };
			REQUIRE(lhs == rhs);
			REQUIRE(not (rhs != lhs));
			REQUIRE(rhs >= lhs);
			REQUIRE(rhs <= lhs);
			REQUIRE_FALSE(lhs < rhs);
			REQUIRE_FALSE(lhs > rhs);
			
			REQUIRE(lhs == 10);
			REQUIRE(not (10 != lhs));
			REQUIRE(10 >= lhs);
			REQUIRE(10 <= lhs);
			REQUIRE_FALSE(lhs < 10);
			REQUIRE_FALSE(lhs > 10);

			REQUIRE(10 == rhs);
			REQUIRE(not (rhs != 10));
			REQUIRE(rhs >= 10);
			REQUIRE(rhs <= 10);
			REQUIRE_FALSE(10 < rhs);
			REQUIRE_FALSE(10 > rhs);
		}
		SECTION("not equal")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE(lhs != rhs);
			REQUIRE(not (rhs == lhs));
			REQUIRE(not (rhs > lhs));
			REQUIRE(not (rhs < lhs));
			REQUIRE(not (lhs <= rhs));
			REQUIRE(not (lhs >= rhs));

			REQUIRE(lhs != 'a');
			REQUIRE(not ('a' == lhs));
			REQUIRE(not ('a' > lhs));
			REQUIRE(not ('a' < lhs));
			REQUIRE(not (lhs <= 'a'));
			REQUIRE(not (lhs >= 'a'));

			REQUIRE(10 != rhs);
			REQUIRE(not (rhs == 10));
			REQUIRE(not (rhs > 10));
			REQUIRE(not (rhs < 10));
			REQUIRE(not (10 <= rhs));
			REQUIRE(not (10 >= rhs));
		}
		SECTION("greater")
		{
			Variant<int, char> lhs{ Elem{12} };
			Variant<int, char> rhs{ Elem{10} };
			REQUIRE(lhs > rhs);
			REQUIRE(rhs < lhs);
			REQUIRE(not (lhs <= rhs));
			REQUIRE(not (rhs >= lhs));
			REQUIRE(lhs != rhs);
			REQUIRE(not (lhs == rhs));

			REQUIRE(lhs > 10);
			REQUIRE(10 < lhs);
			REQUIRE(not (lhs <= 10));
			REQUIRE(not (10 >= lhs));
			REQUIRE(lhs != 10);
			REQUIRE(not (lhs == 10));

			REQUIRE(12 > rhs);
			REQUIRE(rhs < 12);
			REQUIRE(not (12 <= rhs));
			REQUIRE(not (rhs >= 12));
			REQUIRE(12 != rhs);
			REQUIRE(not (12 == rhs));
		}
		SECTION("less")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{12} };
			REQUIRE(lhs < rhs);
			REQUIRE(rhs > lhs);
			REQUIRE(not (lhs >= rhs));
			REQUIRE(not (rhs <= lhs));
			REQUIRE(lhs != rhs);
			REQUIRE(not (lhs == rhs));

			REQUIRE(lhs < 12);
			REQUIRE(12 > lhs);
			REQUIRE(not (lhs >= 12));
			REQUIRE(not (12 <= lhs));
			REQUIRE(lhs != 12);
			REQUIRE(not (lhs == 12));

			REQUIRE(10 < rhs);
			REQUIRE(rhs > 10);
			REQUIRE(not (10 >= rhs));
			REQUIRE(not (rhs <= 10));
			REQUIRE(10 != rhs);
			REQUIRE(not (10 == rhs));
		}
		SECTION("greater_equal")
		{
			SECTION("equal")
			{
				Variant<int, char> lhs{ Elem{10} };
				Variant<int, char> rhs{ Elem{10} };
				REQUIRE(lhs >= rhs);
				REQUIRE(rhs <= lhs);
				REQUIRE(not (lhs < rhs));
				REQUIRE(not (rhs > lhs));

				REQUIRE(lhs >= 10);
				REQUIRE(10 <= lhs);
				REQUIRE(not (lhs < 10));
				REQUIRE(not (10 > lhs));

				REQUIRE(10 >= rhs);
				REQUIRE(rhs <= 10);
				REQUIRE(not (10 < rhs));
				REQUIRE(not (rhs > 10));
			}
			SECTION("greater")
			{
				Variant<int, char> lhs{ Elem{12} };
				Variant<int, char> rhs{ Elem{10} };
				REQUIRE(lhs >= rhs);
				REQUIRE(rhs <= lhs);
				REQUIRE(not (lhs < rhs));
				REQUIRE(not (rhs > lhs));

				REQUIRE(lhs >= 10);
				REQUIRE(10 <= lhs);
				REQUIRE(not (lhs < 10));
				REQUIRE(not (10 > lhs));

				REQUIRE(12 >= rhs);
				REQUIRE(rhs <= 12);
				REQUIRE(not (12 < rhs));
				REQUIRE(not (rhs > 12));
			}
		}
		SECTION("less_equal")
		{
			SECTION("equal")
			{
				Variant<int, char> lhs{ Elem{10} };
				Variant<int, char> rhs{ Elem{10} };
				REQUIRE(lhs <= rhs);
				REQUIRE(rhs >= lhs);
				REQUIRE(not (lhs > rhs));
				REQUIRE(not (rhs < lhs));

				REQUIRE(lhs <= 10);
				REQUIRE(10 >= lhs);
				REQUIRE(not (lhs > 10));
				REQUIRE(not (10 < lhs));

				REQUIRE(10 <= rhs);
				REQUIRE(rhs >= 10);
				REQUIRE(not (10 > rhs));
				REQUIRE(not (rhs < 10));
			}
			SECTION("less")
			{
				Variant<int, char> lhs{ Elem{10} };
				Variant<int, char> rhs{ Elem{12} };
				REQUIRE(lhs <= rhs);
				REQUIRE(rhs >= lhs);
				REQUIRE(not (lhs > rhs));
				REQUIRE(not (rhs < lhs));

				REQUIRE(lhs <= 12);
				REQUIRE(12 >= lhs);
				REQUIRE(not (lhs > 12));
				REQUIRE(not (12 < lhs));

				REQUIRE(10 <= rhs);
				REQUIRE(rhs >= 10);
				REQUIRE(not (10 > rhs));
				REQUIRE(not (rhs < 10));
			}
		}
	}

	SECTION("non-valid")
	{
		SECTION("equal")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE_FALSE(lhs == rhs);
		}
		SECTION("not equal")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE(lhs != rhs);
		}
		SECTION("greater")
		{
			Variant<int, char> lhs{ Elem{12} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE_FALSE(lhs > rhs);
		}
		SECTION("less")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE_FALSE(lhs < rhs);
		}
		SECTION("greater_equal")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE_FALSE(lhs >= rhs);
		}
		SECTION("less_equal")
		{
			Variant<int, char> lhs{ Elem{10} };
			Variant<int, char> rhs{ Elem{'a'} };
			REQUIRE_FALSE(lhs <= rhs);
		}
	}
}

TEST_CASE("Variant can copy construct")
{
	Hz::Variant<int, std::string, bool> variant{Hz::Elem{std::string("hello")}};
	Hz::Variant<int, std::string, bool> other = variant;
	SUCCEED("Constructed");
}

TEST_CASE("Variant can move construct")
{
	Hz::Variant<int, std::string, bool> variant{Hz::Elem{std::string("hello")}};
	Hz::Variant<int, std::string, bool> other = std::move(variant);
	SUCCEED("Constructed");
}

TEST_CASE("Variant can copy assign")
{
	Hz::Variant<int, std::string, bool> variant{Hz::Elem{std::string("hello")}};
	Hz::Variant<int, std::string, bool> other{Hz::Elem{0}};

	other = variant;
	SUCCEED("Constructed");
}

TEST_CASE("Variant can move assign")
{
	Hz::Variant<int, std::string, bool> variant{Hz::Elem{std::string("hello")}};
	Hz::Variant<int, std::string, bool> other{Hz::Elem{0}};

	other = variant;
	SUCCEED("Constructed");
}

TEST_CASE("Variant can be compared")
{
	Hz::Variant<int, char, bool> variant{Hz::Elem{'a'}};
	Hz::Variant<int, char, bool> other = variant;

	REQUIRE(variant == other);
	REQUIRE(not (variant < other));
	REQUIRE(not (variant > other));
	REQUIRE(variant <= other);
	REQUIRE(variant >= other);

	other.Emplace(Hz::Elem{false});
	REQUIRE(variant != other);
	REQUIRE(not (variant < other));
	REQUIRE(not (variant > other));
	REQUIRE(not (variant <= other));
	REQUIRE(not (variant >= other));

	other.Emplace(Hz::Elem{'m'});
	REQUIRE(other != variant);
	REQUIRE((other > variant));
	REQUIRE(not (other < variant));
	REQUIRE(not (other <= variant));
	REQUIRE(other >= variant);
}

TEST_CASE( "Variant supports multi-visitation" )
{
	Hz::Variant<int, char, bool *> a{ Hz::Elem{123} };
	Hz::Variant<int, char, bool *> b{ Hz::Elem{'e'} };

	bool success = false;
	Visit([&](int a_value, char b_value) noexcept
	{
		REQUIRE(a_value == 123);
		REQUIRE(b_value == 'e');
		success = true;
	}, a, b);
	REQUIRE(success);
	REQUIRE_THROWS_AS(Visit([&](bool * a_value, bool * b_value){ return 0; }, a, b), std::bad_variant_access);
}

TEST_CASE( "Variant supports multi-visitation with non-throwing fallback" )
{
	Hz::Variant<int, char, bool *> a{ Hz::Elem{123} };
	Hz::Variant<int, char, bool *> b{ Hz::Elem{'e'} };
	bool success = false;
	Visit([&](int a_value, char b_value) noexcept
	{
		REQUIRE(a_value == 123);
		REQUIRE(b_value == 'e');
		success = true;
	}, a, b);
	REQUIRE(success);
	auto const result = Visit([&](bool * a_value, bool * b_value)
	{
		return 11;
	}, []{ return 22; }, a, b);
	REQUIRE(result == 22);
}