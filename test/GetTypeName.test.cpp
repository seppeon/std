#include "Hz/Std/GetTypeName.hpp"
#include <Hz/TL/TLCommon.hpp>
#include <catch2/catch_all.hpp>

TEST_CASE("Get the type name")
{
	REQUIRE(Hz::GetTypeName<int>() == "int");
	REQUIRE(Hz::GetTypeName<Hz::Type<void>>() == "Hz::Type<void>");
}