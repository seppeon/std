from conan import ConanFile
from conan.tools.build import check_min_cppstd
from conan.tools.files import load
from conan.tools.cmake import CMakeToolchain, CMakeDeps, CMake, cmake_layout
from conan.tools.build import can_run
import os
import re

required_conan_version = ">=1.50.0"

class StdRecipe(ConanFile):
    implements = ["auto_shared_fpic"]
    name = "hz_std"

    def set_version(self):
        content = load(self, os.path.join(self.recipe_folder, "CMakeLists.txt"))
        version = re.search("project\((.*) VERSION (.*) LANGUAGES CXX", content).group(2)
        self.version = version.strip()

    # Optional metadata
    license = "MIT"
    author = "David Ledger davidledger@live.com.au"
    url = f"https://gitlab.com/seppeon/{name}"
    homepage = f"https://seppeon.gitlab.io/{name}/"
    description = "A library that allows viewing of multiple columns of data with a single index."
    topics = ("data oriented programming")
    build_policy = "missing"
    no_copy_source = True
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "CMakeLists.txt", "README.md", "LICENSE.txt", "docs/doxygen/*", "src/*", "cmake/*", "test/*", "test_package/*", "include/*"
   
    def requirements(self):
        self.requires("catch2/3.4.0")
        self.requires("hz_tl/[~0.4.1]", transitive_headers=True)
        self.requires("hz_traits/[~0.4.1]", transitive_headers=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def layout(self):
        cmake_layout(self)

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, "20")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "Hz_Std")
        self.cpp_info.set_property("cmake_target_name", "Hz::Std")
        self.cpp_info.set_property("pkg_config_name", "Hz_Std")
        self.cpp_info.includedirs = ['include']
        self.cpp_info.libs = ["Hz_Std"]
        self.cpp_info.bindirs = ["lib"]
        self.cpp_info.libdirs = ["lib"]